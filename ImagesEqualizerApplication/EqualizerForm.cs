﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Media;
using System.Threading;
using System.Windows.Forms;

using ImagesEqualizerApplication.Properties;
using ImagesEqualizerApplication.Utils;
using ImgModLib.Graphic;
using ImgModLib.Graphic.Filters.Order.Apertures;
using ImgModLib.Mathematic;
using WinControls;

namespace ImagesEqualizerApplication
{
    public partial class EqualizerForm : Form
    {    	
        delegate void WaitBarDelegate();

        struct Property
        {
            public Bitmap bkg;
            public string operation, firstname, secondname, gradtransform, filtr;
            public double koef;
            public string[] gradTransfPlaneNames;	
            public string[] filtrPlaneNames;
            public int size;
            public double parametr;
            public ApertureType apperture;

            public Property(Bitmap bkg, 
                            string operation, 
                            string firstname, 
                            string secondname, 
                            double koef,
                            string gradtransform,
                            string filtr,
                            string[] gradTransfPlaneNames,
                            string[] filtrPlaneNames,
                            int size,
                            double parametr,
                            ApertureType apperture)
            {
                this.bkg = bkg;
                this.operation = operation;
                this.firstname = firstname;
                this.secondname = secondname;
                this.koef = koef;
                this.gradtransform = gradtransform;
                this.filtr = filtr;
                this.gradTransfPlaneNames = gradTransfPlaneNames;
                this.filtrPlaneNames = filtrPlaneNames;
                this.size = size;
                this.parametr = parametr;
                this.apperture = apperture;
            }
        }

        struct PointProperty
        {
            public static int count = 0;

            public Rectangle rect;
            public List<Point> points;

            public PointProperty(Rectangle rect, List<Point> points)
            {
                this.rect = rect;
                this.points = points;
                count++;
            }
        }

        Bitmap imgbufer, gistbufer;
        ImageEx image;
        Gistogram RGistogram, GGistogram, BGistogram;
        int curfilter = 1;
        ImageFormat imageFormat;
        string filename;
        PictureLabel SelPictLabel = null;
        bool needWait = false;
        OpenFileDialog opendialog = new OpenFileDialog();
        SaveFileDialog savedialog = new SaveFileDialog();
        List<PointF> points = null;
        bool draggingMouse = false;
        Pen pen;
        Graphics panelGraphcs;
        Polygon polygon = null;
        Point prevScrollPosinion;
        MessageFilter messageFilter = new MessageFilter();

        public EqualizerForm()
        {
            string filterstring;
            
            InitializeComponent();
            Icon = Properties.Resources.EqualizerIcon;

            OperationsComboBox.SelectedIndex = 0;
            GradTransfComboBox.SelectedIndex = 0;
            AppertureComboBox.SelectedIndex = 0;
            FilterComboBox.SelectedIndex = 0;

            opendialog.Title = "Открыть изображение";
            opendialog.InitialDirectory = "Рабочий стол";
            opendialog.AddExtension = true;
            opendialog.CheckFileExists = true;
            opendialog.CheckPathExists = true;
            
            filterstring = "BMP (*.bmp)|*.bmp|"
                         + "JPEG (*.jpg;*.jpeg;*.jpe;*.jfif)|*.jpg;*.jpeg;*.jpe;*.jfif|"
                         + "GIF (*.gif)|*.gif|"
                         + "TIFF (*.tif;*.tiff)|*.tif;*.tiff|"
                         + "PNG (*.png)|*.png";

            opendialog.Filter = filterstring;
            opendialog.FilterIndex = curfilter;

            savedialog.Title = "Сохранить изображение как..";
            savedialog.CheckFileExists = false;
            savedialog.CheckPathExists = true;
            savedialog.CreatePrompt = true;
            savedialog.OverwritePrompt = true;
            
            savedialog.Filter = filterstring;
            savedialog.FilterIndex = curfilter;

            pen = new Pen(new SolidBrush(Color.Black), 1.5f);
            pen.DashStyle = DashStyle.Dot;
        }

        private void OpenMenuItem_Click(object sender, EventArgs e)
        {            
            if (opendialog.ShowDialog(this) == DialogResult.Cancel) return;
            Invalidate();
            Update();

            curfilter = opendialog.FilterIndex;

            switch (opendialog.FilterIndex)
            {
                case 1: imageFormat = ImageFormat.Bmp; break;
                case 2: imageFormat = ImageFormat.Jpeg; break;
                case 3: imageFormat = ImageFormat.Gif; break;
                case 4: imageFormat = ImageFormat.Tiff; break;
                case 5: imageFormat = ImageFormat.Png; break;
            }

            if (image == null)
                ImagePanel.CreateGraphics().Clear(Color.White);
            else 
                DrawImageOnPanel(ScaleCheckBox.Checked);

            filename = opendialog.FileName;
            LoadImageBkgWorker.RunWorkerAsync(filename);
            Thread.Sleep(Util.DELAY_BEFORE_PROGRESS_SHOW_MS);

            ActivateProgressBar();
        }
        
        void LoadImageInBackground(object sender, DoWorkEventArgs e)
        {
            needWait = true;

            string filename = (string)e.Argument;

            image = new ImageEx(filename);
            UpdateGistograms();

            needWait = false;        	
        }
        
        void BackgroungImageLoadingIsComplete(object sender, RunWorkerCompletedEventArgs e)
        {
            ImagePanel.Image = RGistogramPanel.Image = GGistogramPanel.Image = BGistogramPanel.Image = (Image) image;

            points = null;
            polygon = null;
            SetInterfaceState(true);
            DrawImageOnPanel(ScaleCheckBox.Checked);
            DrawGistograms();

            GC.Collect();        	
        }        

        void SetInterfaceState(bool state)
        {
            ScaleCheckBox.Enabled = state;
            ScaleCheckBox.Checked = state;
            ColorSchemasMenuItem.Enabled = state;
            
            SaveMenuItem.Enabled = state;
            SaveAsMenuItem.Enabled = state;
            ApplyButton.Enabled = state;
            BackgroundsPanel.Enabled = state;
            BackgroundsLabel.Enabled = state;
            OperationsLabel.Enabled = state;
            OperationsComboBox.Enabled = state;
            GradTransfLabel.Enabled = state;
            GradTransfComboBox.Enabled = state;
            FilterComboBox.Enabled = state;
            FilterLabel.Enabled = state;
            imageMenuItem.Enabled = state;
        }

        void DrawImageOnPanel(bool NeedScaling)
        {            
            Graphics g = Graphics.FromImage(imgbufer);
            Graphics panelgraph = Graphics.FromHwnd(ImagePanel.Handle);
            SolidBrush brush = new SolidBrush(ImagePanel.BackColor);

            if (NeedScaling)
            {
                double k, kx, ky;

                kx = ImagePanel.Width / (double)image.Width;
                ky = ImagePanel.Height / (double)image.Height;

                if (ky < kx)
                {
                    k = ky;

                    int drawWidth = (int)(image.Width * k);
                    int drawHeigth = (int)(image.Height * k);

                    int drawX = (ImagePanel.Width - drawWidth) / 2;

                    g.FillRectangle(brush, 0, 0, drawX, imgbufer.Height);
                    image.Draw(drawX, 0, drawWidth, drawHeigth, g);
                    g.FillRectangle(brush, imgbufer.Width - drawX - 1, 0, drawX, imgbufer.Height);
                }
                else
                {
                    k = kx;

                    int drawWidth = (int)(image.Width * k);
                    int drawHeigth = (int)(image.Height * k);

                    int drawY = (ImagePanel.Height - drawHeigth) / 2;
                                        
                    g.FillRectangle(brush, 0, 0, imgbufer.Width, drawY);
                    image.Draw(0, drawY, drawWidth, drawHeigth, g);
                    g.FillRectangle(brush, 0, imgbufer.Height - drawY - 1, imgbufer.Width, drawY);
                }

            }
            else
            {
                g.Clear(ImagePanel.BackColor);
                image.Draw(-ImagePanel.AutoScrollPosition.X, -ImagePanel.AutoScrollPosition.Y,
                           ImagePanel.Width, ImagePanel.Height, 0, 0, ImagePanel.Width, ImagePanel.Height,
                           g);

                if (polygon != null)
                    polygon.Draw(Color.Black, 1.5f, DashStyle.Dot, g, false);
            }

            panelgraph.DrawImageUnscaled(imgbufer, 0, 0);
                        
            g.Dispose();
            g = null;
            GC.Collect();
        }

        private void ImagePanel_Paint(object sender, PaintEventArgs e)
        {
            if (image == null || ImagePanel.Width == 0 || ImagePanel.Height == 0) return;

            DrawImageOnPanel(ScaleCheckBox.Checked);

        }

        private void SaveMenuItem_Click(object sender, EventArgs e)
        {
            image.Save(filename, imageFormat);
        }

        private void ColorSchemasMenuItem_Click(object sender, EventArgs e)
        {
            ColorSchemsForm form = new ColorSchemsForm((ImageEx)image.Clone(), this);
            Hide();

            DialogResult rez = form.ShowDialog(this);

            Location = form.Location;
            Size = form.Size;
            
            form.Dispose();
            Show();

            if (rez == DialogResult.OK)
            {
                Invalidate();
                Update();
                image = form.Image;

                Thread thread = new Thread(new ThreadStart(UpdateGistograms));
                thread.IsBackground = true;
                thread.Start();
                Thread.Sleep(Util.DELAY_BEFORE_PROGRESS_SHOW_MS);

                ActivateProgressBar();

                DrawImageOnPanel(ScaleCheckBox.Checked);
                DrawGistograms();
            }

            GC.Collect();
        }

        void UpdateGistograms()
        {
            needWait = true;

            RGistogram = new Gistogram(image.Planes["R"], Color.Red);
            GGistogram = new Gistogram(image.Planes["G"], Color.Green);
            BGistogram = new Gistogram(image.Planes["B"], Color.Blue);

            needWait = false;
        }

        private void ScaleCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (!ScaleCheckBox.Checked)
            {
                ImagePanel.AutoScroll = true;
                ImagePanel.AutoScrollMinSize = image.ImageSize;
                ImagePanel.CreateGraphics().Clear(ImagePanel.BackColor);
            }
            else
            {
                ImagePanel.AutoScroll = false;
                ImagePanel.AutoScrollMinSize = Size.Empty;
                polygon = null;
                points = null;
                image.RegionOfInterest = null;
            }
            DrawImageOnPanel(ScaleCheckBox.Checked);
        }

        private void PictureLabel_MouseClick(object sender, MouseEventArgs e)
        {
            PictureLabel NewPictLabel = (PictureLabel)sender;

            if (e.Button == MouseButtons.Left && !NewPictLabel.Selected)
            {
                NewPictLabel.Selected = true;

                if (SelPictLabel != null)
                    SelPictLabel.Selected = false;

                SelPictLabel = NewPictLabel;
            }
            else if (e.Button == MouseButtons.Right && NewPictLabel.Selected)
            {
                NewPictLabel.Selected = false;
                SelPictLabel = null;
            }
        }

        string GetPlaneName(GroupBox groupbox)
        {
            RadioButton selRadiobutton = null;

            foreach (RadioButton rb in groupbox.Controls)
            {
                if (rb.Checked)
                {
                    selRadiobutton = rb;
                    break;
                }
            }

            return selRadiobutton.Text;
        }

        string[] GetSelPlanes(GroupBox groupbox)
        {
            List<string> rez = new List<string>();

            foreach (CheckBox checkbox in groupbox.Controls)
            {
                if (checkbox.Checked) rez.Add(checkbox.Text);
            }

            return rez.ToArray();
        }

        void ApplyChangesToImage(Object obj)
        {
            ImageEx copyImage;
            needWait = true;

            lock (image)
            {
                copyImage = (ImageEx)image.Clone();
            }            
            
            Property property = (Property)obj;

            bool needrepaint = false;

            if (property.bkg != null)
            {
                ImageEx background = new ImageEx(property.bkg);

                copyImage = copyImage.PutBackground(background, 0.45, 0.55);
                copyImage.Eclipse();
                needrepaint = true;
            }

            if (property.operation != "--нет--")
            {
                string firstName = property.firstname;
                string secondName = property.secondname;
                double koef = property.koef;

                switch (property.operation)
                {
                    case "Присвоить плоскости":
                        copyImage.Planes[firstName] = copyImage.Planes[firstName].Equate(copyImage.Planes[secondName]);
                        break;
                    case "Сложить плоскости":
                        copyImage.Planes[firstName] = copyImage.Planes[firstName].Add(copyImage.Planes[secondName]);
                        break;
                    case "Вычесть плоскости":
                        copyImage.Planes[firstName] = copyImage.Planes[firstName].Sub(copyImage.Planes[secondName]);
                        break;
                    case "Перемножить плоскости":
                        copyImage.Planes[firstName] = copyImage.Planes[firstName].Mul(copyImage.Planes[secondName]);
                        break;
                    case "Умножить на скаляр":
                        copyImage.Planes[firstName] = copyImage.Planes[firstName].Mul(koef);
                        break;
                }

                copyImage.Eclipse();
                needrepaint = true;
            }

            if (property.gradtransform != "--нет--" && property.gradTransfPlaneNames.Length > 0)
            {
                GradationTransform GradTransfR, GradTransfG, GradTransfB, gt;
                GradTransfR = GradTransfG = GradTransfB = null;

                switch (property.gradtransform)
                {
                    case "Негатив":
                        gt = new GradationTransform(TransformType.Negative);

                        if (property.gradTransfPlaneNames.Contains("R")) GradTransfR = gt;
                        if (property.gradTransfPlaneNames.Contains("G")) GradTransfG = gt;
                        if (property.gradTransfPlaneNames.Contains("B")) GradTransfB = gt;
                        break;
                    
                    case "Логарифмическое":
                        gt = new GradationTransform(TransformType.Logarithm);

                        if (property.gradTransfPlaneNames.Contains("R")) GradTransfR = gt;
                        if (property.gradTransfPlaneNames.Contains("G")) GradTransfG = gt;
                        if (property.gradTransfPlaneNames.Contains("B")) GradTransfB = gt;
                        break;
                    
                    case "Соляризация":
                        gt = new GradationTransform(TransformType.Solarization);

                        if (property.gradTransfPlaneNames.Contains("R")) GradTransfR = gt;
                        if (property.gradTransfPlaneNames.Contains("G")) GradTransfG = gt;
                        if (property.gradTransfPlaneNames.Contains("B")) GradTransfB = gt;
                        break;

                    case "Контраст":
                        gt = new GradationTransform(TransformType.Contrast);

                        if (property.gradTransfPlaneNames.Contains("R")) GradTransfR = gt;
                        if (property.gradTransfPlaneNames.Contains("G")) GradTransfG = gt;
                        if (property.gradTransfPlaneNames.Contains("B")) GradTransfB = gt;
                        break;
    
                    case "Повысить яркость":
                        gt = new GradationTransform(1d / 4);

                        if (property.gradTransfPlaneNames.Contains("R")) GradTransfR = gt;
                        if (property.gradTransfPlaneNames.Contains("G")) GradTransfG = gt;
                        if (property.gradTransfPlaneNames.Contains("B")) GradTransfB = gt;
                        break;

                    case "Понизить яркость":
                        gt = new GradationTransform(-1d / 4);

                        if (property.gradTransfPlaneNames.Contains("R")) GradTransfR = gt;
                        if (property.gradTransfPlaneNames.Contains("G")) GradTransfG = gt;
                        if (property.gradTransfPlaneNames.Contains("B")) GradTransfB = gt;
                        break;

                    case "Эквализация":
                        if (property.gradTransfPlaneNames.Contains("R"))
                            GradTransfR = new GradationTransform(RGistogram);

                        if (property.gradTransfPlaneNames.Contains("G"))
                            GradTransfG = new GradationTransform(GGistogram);

                        if (property.gradTransfPlaneNames.Contains("B"))
                            GradTransfB = new GradationTransform(BGistogram);
                        break;
                }

                copyImage.ApplyGradTransform(GradTransfR, GradTransfG, GradTransfB);

                copyImage.Eclipse();
                needrepaint = true;
            }

            if (property.filtr != "--нет--" && property.filtrPlaneNames.Length > 0)
            {
                SpaceFilterType filtertype = SpaceFilterType.UserFilter;
                OrdderFilterType filtertype2 = OrdderFilterType.Median; 

                switch (property.filtr)
                {
                    case "Усредняющий": filtertype = SpaceFilterType.Averaging; break;
                    case "Треугольный": filtertype = SpaceFilterType.Triangle; break;
                    case "Превита(горизонтальный)": filtertype = SpaceFilterType.PrevitHorizontal; break;
                    case "Превита(вертикальный)": filtertype = SpaceFilterType.PrevitVertical; break;
                    case "Собеля(горизонтальный)": filtertype = SpaceFilterType.SobelHorizontal; break;
                    case "Собеля(вертикальный)": filtertype = SpaceFilterType.SobelVertical; break;
                    case "Высокочастотный": filtertype = SpaceFilterType.HighFrequency; break;
                    case "Гаусса": filtertype = SpaceFilterType.Gaussian; break;
                    case "Поднятия высоких частот": filtertype = SpaceFilterType.UpperHighFrequency; break;
                    case "Нерезкое маскирование": filtertype = SpaceFilterType.Diffusing; break;
                    case "Медианный": filtertype2 = OrdderFilterType.Median; break;
                    case "Минимизирующий": filtertype2 = OrdderFilterType.Minimum; break;
                    case "Максимизирующий": filtertype2 = OrdderFilterType.Maximum; break;
                }

                if (property.filtr != "Медианный" 
                    && property.filtr != "Минимизирующий" 
                    && property.filtr != "Максимизирующий")
                {
                    SpaceFilterMasck masck, RedPlaneMasck, GreenPlaneMasck, BluePlaneMasck;
                    RedPlaneMasck = GreenPlaneMasck = BluePlaneMasck = null;

                    masck = new SpaceFilterMasck(filtertype, property.size, property.parametr);

                    if (property.filtrPlaneNames.Contains("R")) RedPlaneMasck = masck;
                    if (property.filtrPlaneNames.Contains("G")) GreenPlaneMasck = masck;
                    if (property.filtrPlaneNames.Contains("B")) BluePlaneMasck = masck;
                    
                    copyImage.ApplyFilter(RedPlaneMasck, GreenPlaneMasck, BluePlaneMasck);
                }
                else 
                {
                    OrderFilterMasck masck, RedPlaneMasck, GreenPlaneMasck, BluePlaneMasck;
                    RedPlaneMasck = GreenPlaneMasck = BluePlaneMasck = null;

                    masck = new OrderFilterMasck(property.size, property.apperture);

                    if (property.filtrPlaneNames.Contains("R")) RedPlaneMasck = masck;
                    if (property.filtrPlaneNames.Contains("G")) GreenPlaneMasck = masck;
                    if (property.filtrPlaneNames.Contains("B")) BluePlaneMasck = masck;

                    copyImage.ApplyFilter(RedPlaneMasck, GreenPlaneMasck, BluePlaneMasck, filtertype2);
                }

                copyImage.Eclipse();
                needrepaint = true;
            }
            
            if (!needrepaint)
            {
                needWait = false;
                return;
            }
            
            copyImage.Update();

            lock (RGistogram)
            {
                RGistogram = new Gistogram(copyImage.Planes["R"], Color.Red);
                GGistogram = new Gistogram(copyImage.Planes["G"], Color.Green);
                BGistogram = new Gistogram(copyImage.Planes["B"], Color.Blue);
            }

            lock (image)
            {
                image = copyImage;                
            }

            needWait = false;
            GC.Collect();
        }

        private void ActivateProgressBar()
        {
            Cursor = Cursors.WaitCursor;

            WaitProgressBar.Value = 0;

            WaitProgressBar.Size = new Size(ImagePanel.Width / 2, WaitProgressBar.Height);
            int BarX = (ImagePanel.Width - WaitProgressBar.Width) / 2;
            int BarY = (ImagePanel.Height - WaitProgressBar.Height) / 2;
            WaitProgressBar.Location = new Point(BarX, BarY);
            WaitProgressBar.Visible = true;

            WaitProgressBar.Invalidate();
            WaitProgressBar.Update();

            while (needWait)
            {
                WaitProgressBar.Invalidate();
                WaitProgressBar.Update();
                Thread.Sleep(Util.DELAY_BEFORE_PROGRESS_SHOW_MS);
            }

            WaitProgressBar.Visible = false;

            Cursor = Cursors.Default;
        }

        delegate void pint();

        void BarthreadFunc()
        {
            WaitBarDelegate processDelegate;

            Invoke(new WaitBarDelegate(BeginProgressBar));

            processDelegate = new WaitBarDelegate(ProcessBar);
            Invoke(processDelegate);
            Thread.Sleep(Util.DELAY_BEFORE_PROGRESS_SHOW_MS);

            while (needWait)
            {
                Invoke(processDelegate);
                Thread.Sleep(Util.DELAY_BEFORE_PROGRESS_SHOW_MS);
            }

            Invoke(new WaitBarDelegate(EndProgressBar));

            Invoke(new pint(p));
        }



        void BeginProgressBar()
        {
            Application.AddMessageFilter(messageFilter);

            Cursor = Cursors.WaitCursor;

            WaitProgressBar.Value = 0;            
            WaitProgressBar.Size = new Size(ImagePanel.Width / 2, WaitProgressBar.Height);
            int BarX = (ImagePanel.Width - WaitProgressBar.Width) / 2;
            int BarY = (ImagePanel.Height - WaitProgressBar.Height) / 2;
            WaitProgressBar.Location = new Point(BarX, BarY);
            WaitProgressBar.Visible = true;
        }

        void ProcessBar()
        {
            WaitProgressBar.Invalidate();
            WaitProgressBar.Update();
            Invalidate(true);
            Update();
        }

        void EndProgressBar()
        {
            WaitProgressBar.Visible = false;

            Cursor = Cursors.Default;
            Application.RemoveMessageFilter(messageFilter);
        }

        void p()
        {
            DrawImageOnPanel(ScaleCheckBox.Checked);
            DrawGistograms();
        }

        private void ApplyButton_Click(object sender, EventArgs e)
        {
            Thread barThread = new Thread(new ThreadStart(BarthreadFunc));
            Thread thread = new Thread(new ParameterizedThreadStart(ApplyChangesToImage));

            thread.IsBackground = barThread.IsBackground = true;

            Property property = new Property(null,
                                             OperationsComboBox.Text,
                                             GetPlaneName(FirstGroupBox),
                                             GetPlaneName(SecondGroupBox),
                                             (double)MultiplierUpDown.Value,
                                             GradTransfComboBox.Text,
                                             FilterComboBox.Text,
                                             GetSelPlanes(GradTransfGroupBox),
                                             GetSelPlanes(FilterGroupBox),
                                             (int)SizeUpDown.Value,
                                             (double)ParamUpDown.Value,
                                             (ApertureType)(AppertureComboBox.SelectedIndex + 1));

            if (SelPictLabel != null)
                property.bkg = (Bitmap)SelPictLabel.Picture;
                       
            thread.Start(property);
            Thread.Sleep(Util.DELAY_BEFORE_PROGRESS_SHOW_MS);
            
            barThread.Priority = ThreadPriority.Lowest;
            barThread.Start();
        }

        void DrawGistograms()
        {            
            Graphics g = Graphics.FromImage(gistbufer);
            Rectangle scalerect = new Rectangle(new Point(0,0), 
                                                new Size(RGistogramPanel.Width - 3, RGistogramPanel.Height));

            g.Clear(Color.Black);
            RGistogram.Draw(0, 0, RGistogramPanel.Width, RGistogramPanel.Height, g);
            RGistogramPanel.CreateGraphics().DrawImage(gistbufer, scalerect);

            g.Clear(Color.Black);
            GGistogram.Draw(0, 0, GGistogramPanel.Width, GGistogramPanel.Height, g);
            GGistogramPanel.CreateGraphics().DrawImage(gistbufer, scalerect);

            g.Clear(Color.Black);
            BGistogram.Draw(0, 0, BGistogramPanel.Width, BGistogramPanel.Height, g);
            BGistogramPanel.CreateGraphics().DrawImage(gistbufer, scalerect);
        }

        private void GistogramsTableLayout_Paint(object sender, PaintEventArgs e)
        {
            if (RGistogram == null || GGistogram == null || BGistogram == null) return;

            DrawGistograms();
        }

        private void OperationsComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (((ComboBox)sender).Text)
            {
                case "--нет--":
                    FirstGroupBox.Visible = false;
                    SecondGroupBox.Visible = false;
                    OperationSign.Visible = false;
                    MultiplierUpDown.Visible = false;
                    break;

                case "Присвоить плоскости":
                    FirstGroupBox.Visible = true;
                    SecondGroupBox.Visible = true;
                    OperationSign.Visible = true;
                    OperationSign.Text = "=";
                    MultiplierUpDown.Visible = false;
                    FirstR_RadioBut.Checked = SecondR_RadioBut.Checked = true;
                    break;

                case "Сложить плоскости":
                    FirstGroupBox.Visible = true;
                    SecondGroupBox.Visible = true;
                    OperationSign.Visible = true;
                    OperationSign.Text = "+";
                    MultiplierUpDown.Visible = false;
                    FirstR_RadioBut.Checked = SecondR_RadioBut.Checked = true;
                    break;

                case "Вычесть плоскости":
                    FirstGroupBox.Visible = true;
                    SecondGroupBox.Visible = true;
                    OperationSign.Visible = true;
                    OperationSign.Text = "-";
                    MultiplierUpDown.Visible = false;
                    FirstR_RadioBut.Checked = SecondR_RadioBut.Checked = true;
                    break;

                case "Перемножить плоскости":
                    FirstGroupBox.Visible = true;
                    SecondGroupBox.Visible = true;
                    OperationSign.Visible = true;
                    OperationSign.Text = "*";
                    MultiplierUpDown.Visible = false;
                    FirstR_RadioBut.Checked = SecondR_RadioBut.Checked = true;
                    break;

                case "Умножить на скаляр":
                    FirstGroupBox.Visible = true;
                    SecondGroupBox.Visible = false;
                    OperationSign.Visible = true;
                    OperationSign.Text = "*";
                    MultiplierUpDown.Visible = true;
                    FirstR_RadioBut.Checked = SecondR_RadioBut.Checked = true;
                    break;
            }
        }

        private void GradTransfComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (((ComboBox)sender).Text == "--нет--")
                GradTransfGroupBox.Visible = false;
            else
                GradTransfGroupBox.Visible = true;
        }

        private void SaveAsMenuItem_Click(object sender, EventArgs e)
        {
            savedialog.FilterIndex = curfilter;

            if (savedialog.ShowDialog(this) == DialogResult.Cancel) return;
            Invalidate();
            Update();

            switch (savedialog.FilterIndex)
            {
                case 1: imageFormat = ImageFormat.Bmp; break;
                case 2: imageFormat = ImageFormat.Jpeg; break;
                case 3: imageFormat = ImageFormat.Gif; break;
                case 4: imageFormat = ImageFormat.Tiff; break;
                case 5: imageFormat = ImageFormat.Png; break;
            }

            curfilter = savedialog.FilterIndex;

            image.Save(savedialog.FileName, imageFormat);
        }
        
        private void ImagePanel_MouseDown(object sender, MouseEventArgs e)
        {
            if (ScaleCheckBox.Checked || e.Button == MouseButtons.Middle || image == null)
                return;

            Rectangle cliprect = new Rectangle(0, 0, 
                                               Math.Min(ImagePanel.Width, image.Width),
                                               Math.Min(ImagePanel.Height, image.Height));            
            Cursor.Clip = ImagePanel.RectangleToScreen(cliprect);
            
            if (e.Button == MouseButtons.Right)
            {
                polygon = null;
                points = null;
                image.RegionOfInterest = null;
                DrawImageOnPanel(ScaleCheckBox.Checked);
            }
            else if (e.Button == MouseButtons.Left && polygon == null)
            {
                draggingMouse = true;
                panelGraphcs = Graphics.FromHwnd(ImagePanel.Handle);

                points = new List<PointF>();
                if (!cliprect.Contains(e.Location)) return;
                points.Add(e.Location);
                               
                panelGraphcs.DrawLine(pen, e.Location, e.Location);
            }

            GC.Collect();
        }

        private void ImagePanel_MouseMove(object sender, MouseEventArgs e)
        {
            if (ScaleCheckBox.Checked || !draggingMouse || image == null) return;

            points.Add(e.Location);

            if (points.Count > 1)
                panelGraphcs.DrawLine(pen, points[points.Count - 2], e.Location);
            else
                panelGraphcs.DrawLine(pen, e.Location, e.Location);
        }

        private void ImagePanel_MouseUp(object sender, MouseEventArgs e)
        {            
            int n;
            int minx, miny, maxx, maxy;            
            List<Point> listPoints;

            Cursor.Clip = Rectangle.Empty;

            if (ScaleCheckBox.Checked || e.Button != MouseButtons.Left || !draggingMouse || image == null) 
                return;

            Cursor = Cursors.WaitCursor;            

            n = points.Count / 10;
            draggingMouse = false;

            while (n > 0 && polygon == null)
            {
                try
                {
                    polygon = new Polygon(points.ToArray());
                }
                catch
                {
                    points.RemoveAt(points.Count - 1);
                    n--;
                }
            }

            if (polygon == null)
            {
                points = null;
                SystemSounds.Beep.Play();
                DrawImageOnPanel(ScaleCheckBox.Checked);
                Cursor = Cursors.Default;
                GC.Collect();

                return;
            }

            minx = (int)polygon.Left;
            miny = (int)polygon.Bottom;
            maxx = (int)polygon.Right;
            maxy = (int)polygon.Top;
            
            if (maxx > image.Width) maxx = image.Width;
            if (maxy > image.Height) maxy = image.Height;

            listPoints = GetListPoints(minx, miny, maxx, maxy);

            Size size = new Size(ImagePanel.AutoScrollPosition);
            listPoints = listPoints.Select(point => point -= size).ToList();

            image.RegionOfInterest = listPoints;

            prevScrollPosinion = ImagePanel.AutoScrollPosition;
            DrawImageOnPanel(ScaleCheckBox.Checked);
            points = null;
            Cursor = Cursors.Default;

            GC.Collect();
        }

        void AddPointsFunc(object obj)
        {
            List<Point> list = ((PointProperty)obj).points;
            
            int minx = ((PointProperty)obj).rect.Left;
            int miny = ((PointProperty)obj).rect.Top;
            int maxx = ((PointProperty)obj).rect.Right;
            int maxy = ((PointProperty)obj).rect.Bottom;

            for (int y = miny; y < maxy; y++)
            {
                for (int x = minx; x < maxx; x++)
                {
                    Point p = new Point(x, y);

                    if (polygon.Contains(p)) lock (list) { list.Add(p); }
                }
            }
            PointProperty.count--;
        }

        private List<Point> GetListPoints(int minx, int miny, int maxx, int maxy)
        {
            List<Point> list = new List<Point>();
            Thread[] threads = new Thread[Environment.ProcessorCount];
            Rectangle[] rects = new Rectangle[Environment.ProcessorCount];

            float dx = (maxx - minx) / Environment.ProcessorCount;

            for (int i = 0, x = minx; i < threads.Length - 1; i++, x = (int)(x + dx))
            {
                threads[i] = new Thread(new ParameterizedThreadStart(AddPointsFunc));
                threads[i].Priority = ThreadPriority.Highest;
                threads[i].IsBackground = true;
                rects[i] = new Rectangle(x, miny, (int)dx, maxy - miny);
                threads[i].Start(new PointProperty(rects[i], list));
            }

            threads[threads.Length - 1] = new Thread(new ParameterizedThreadStart(AddPointsFunc));
            threads[threads.Length - 1].Priority = ThreadPriority.Highest;
            threads[threads.Length - 1].IsBackground = true;
            rects[rects.Length - 1] = new Rectangle(rects[rects.Length - 2].Right,
                                                    miny, maxx - (rects[rects.Length - 2].Right),
                                                    maxy - miny);

            threads[threads.Length - 1].Start(new PointProperty(rects[threads.Length - 1], list));

            while (PointProperty.count > 0);

            return list;
        }

        private void ImagePanel_Scroll(object sender, ScrollEventArgs e)
        {
            if (polygon == null) return;

            switch (e.ScrollOrientation)
            {
                case ScrollOrientation.HorizontalScroll:
                    polygon.ParallelTranslate(e.OldValue - e.NewValue, 0);
                    prevScrollPosinion.X += e.OldValue - e.NewValue;
                    break;

                case ScrollOrientation.VerticalScroll:
                    polygon.ParallelTranslate(0, e.OldValue - e.NewValue);
                    prevScrollPosinion.Y += e.OldValue - e.NewValue;
                    break;
                    
            }
        }

        private void ImagePanel_Resize(object sender, EventArgs e)
        {
            if (ImagePanel.Width > 0 && ImagePanel.Height > 0
                && (ImagePanel.Width != imgbufer.Width || ImagePanel.Height != imgbufer.Height))
                imgbufer = new Bitmap(ImagePanel.Width, ImagePanel.Height);
            
            if (polygon == null) return;

            polygon.ParallelTranslate(ImagePanel.AutoScrollPosition.X - prevScrollPosinion.X,
                                      ImagePanel.AutoScrollPosition.Y - prevScrollPosinion.Y);
            prevScrollPosinion = ImagePanel.AutoScrollPosition;
        }

        private void FilterComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            string text = ((ComboBox)sender).Text;

            switch (text)
            {
                case "--нет--":
                    FilterGroupBox.Visible = false;
                    SizeLabel.Visible = SizeUpDown.Visible = false;
                    ParametrLabel.Visible = ParamUpDown.Visible = false;
                    AppertureLabel.Visible = AppertureComboBox.Visible = false;
                    break;

                case "Усредняющий":
                case "Треугольный":
                case "Превита(горизонтальный)":
                case "Превита(вертикальный)":
                case "Собеля(горизонтальный)":
                case "Собеля(вертикальный)":
                case "Высокочастотный":
                    FilterGroupBox.Visible = true;
                    SizeLabel.Visible = SizeUpDown.Visible = true;
                    ParametrLabel.Visible = ParamUpDown.Visible = false;
                    AppertureLabel.Visible = AppertureComboBox.Visible = false;
                    break;

                case "Гаусса":
                case "Поднятия высоких частот":
                case "Нерезкое маскирование":
                    FilterGroupBox.Visible = true;
                    SizeLabel.Visible = SizeUpDown.Visible = true;
                    ParametrLabel.Visible = ParamUpDown.Visible = true;
                    AppertureLabel.Visible = AppertureComboBox.Visible = false;

                    if (text == "Гаусса")
                        ParametrLabel.Text = "σ:"; 
                    else
                        ParametrLabel.Text = "α:"; 
                    break;

                case "Медианный":
                case "Минимизирующий":
                case "Максимизирующий":
                    FilterGroupBox.Visible = true;
                    SizeLabel.Visible = SizeUpDown.Visible = true;
                    AppertureLabel.Visible = AppertureComboBox.Visible = true;
                    ParametrLabel.Visible = ParamUpDown.Visible = false;
                    break;
            }
        }

        private void AboutMenuItem_Click(object sender, EventArgs e)
        {
            new AboutBox().ShowDialog(this);
            GC.Collect();            
        }

        private void SizeUpDown_ValueChanged(object sender, EventArgs e)
        {
            if (SizeUpDown.Value % 2 == 0) SizeUpDown.Value--;
            if (SizeUpDown.Value < SizeUpDown.Minimum) SizeUpDown.Value = SizeUpDown.Minimum;
        }

        private void EqualizerForm_Load(object sender, EventArgs e)
        {
            imgbufer = new Bitmap(ImagePanel.Width, ImagePanel.Height);
            gistbufer = new Bitmap(RGistogramPanel.Width, RGistogramPanel.Height);
        }

        private void GistogramsTableLayout_Resize(object sender, EventArgs e)
        {
            if (RGistogramPanel.Width > 0 && RGistogramPanel.Height > 0
                && (RGistogramPanel.Width != gistbufer.Width || RGistogramPanel.Height != gistbufer.Height))
                gistbufer = new Bitmap(RGistogramPanel.Width, RGistogramPanel.Height);
        }        
    }
}
