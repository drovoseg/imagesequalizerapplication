﻿namespace ImagesEqualizerApplication
{
    partial class ColorSchemsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        	this.groupBox1 = new System.Windows.Forms.GroupBox();
        	this.Lab_RadioButton = new System.Windows.Forms.RadioButton();
        	this.YUV_RadioButton = new System.Windows.Forms.RadioButton();
        	this.HSV_RadioButton = new System.Windows.Forms.RadioButton();
        	this.XYZ_RadioButton = new System.Windows.Forms.RadioButton();
        	this.CMY_RadioButton = new System.Windows.Forms.RadioButton();
        	this.RGB_RadioButton = new System.Windows.Forms.RadioButton();
        	this.ColorPlane1Label = new System.Windows.Forms.Label();
        	this.ColorPlane1TrackBar = new System.Windows.Forms.TrackBar();
        	this.ColorPlane2TrackBar = new System.Windows.Forms.TrackBar();
        	this.ColorPlane2Label = new System.Windows.Forms.Label();
        	this.ColorPlane3TrackBar = new System.Windows.Forms.TrackBar();
        	this.ColorPlane3Label = new System.Windows.Forms.Label();
        	this.ColorPlane2TextBox = new System.Windows.Forms.TextBox();
        	this.ColorPlane3TextBox = new System.Windows.Forms.TextBox();
        	this.ColorPlane1TextBox = new System.Windows.Forms.TextBox();
        	this.WaitProgressBar = new System.Windows.Forms.ProgressBar();
        	this.panel1 = new System.Windows.Forms.Panel();
        	this.button1 = new System.Windows.Forms.Button();
        	this.ApplyButton = new System.Windows.Forms.Button();
        	this.ImagePanel = new WinControls.PanelEx();
        	this.groupBox1.SuspendLayout();
        	((System.ComponentModel.ISupportInitialize)(this.ColorPlane1TrackBar)).BeginInit();
        	((System.ComponentModel.ISupportInitialize)(this.ColorPlane2TrackBar)).BeginInit();
        	((System.ComponentModel.ISupportInitialize)(this.ColorPlane3TrackBar)).BeginInit();
        	this.panel1.SuspendLayout();
        	this.SuspendLayout();
        	// 
        	// groupBox1
        	// 
        	this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
        	this.groupBox1.BackColor = System.Drawing.SystemColors.GrayText;
        	this.groupBox1.Controls.Add(this.Lab_RadioButton);
        	this.groupBox1.Controls.Add(this.YUV_RadioButton);
        	this.groupBox1.Controls.Add(this.HSV_RadioButton);
        	this.groupBox1.Controls.Add(this.XYZ_RadioButton);
        	this.groupBox1.Controls.Add(this.CMY_RadioButton);
        	this.groupBox1.Controls.Add(this.RGB_RadioButton);
        	this.groupBox1.Location = new System.Drawing.Point(330, 12);
        	this.groupBox1.Name = "groupBox1";
        	this.groupBox1.Size = new System.Drawing.Size(172, 84);
        	this.groupBox1.TabIndex = 0;
        	this.groupBox1.TabStop = false;
        	this.groupBox1.Text = "Цветовая схема";
        	// 
        	// Lab_RadioButton
        	// 
        	this.Lab_RadioButton.AutoSize = true;
        	this.Lab_RadioButton.Location = new System.Drawing.Point(119, 52);
        	this.Lab_RadioButton.Name = "Lab_RadioButton";
        	this.Lab_RadioButton.Size = new System.Drawing.Size(43, 17);
        	this.Lab_RadioButton.TabIndex = 5;
        	this.Lab_RadioButton.Text = "Lab";
        	this.Lab_RadioButton.UseVisualStyleBackColor = true;
        	this.Lab_RadioButton.CheckedChanged += new System.EventHandler(this.RadioButton_CheckedChanged);
        	// 
        	// YUV_RadioButton
        	// 
        	this.YUV_RadioButton.AutoSize = true;
        	this.YUV_RadioButton.Location = new System.Drawing.Point(67, 52);
        	this.YUV_RadioButton.Name = "YUV_RadioButton";
        	this.YUV_RadioButton.Size = new System.Drawing.Size(47, 17);
        	this.YUV_RadioButton.TabIndex = 3;
        	this.YUV_RadioButton.Text = "YUV";
        	this.YUV_RadioButton.UseVisualStyleBackColor = true;
        	this.YUV_RadioButton.CheckedChanged += new System.EventHandler(this.RadioButton_CheckedChanged);
        	// 
        	// HSV_RadioButton
        	// 
        	this.HSV_RadioButton.AutoSize = true;
        	this.HSV_RadioButton.Location = new System.Drawing.Point(119, 26);
        	this.HSV_RadioButton.Name = "HSV_RadioButton";
        	this.HSV_RadioButton.Size = new System.Drawing.Size(47, 17);
        	this.HSV_RadioButton.TabIndex = 4;
        	this.HSV_RadioButton.Text = "HSV";
        	this.HSV_RadioButton.UseVisualStyleBackColor = true;
        	this.HSV_RadioButton.CheckedChanged += new System.EventHandler(this.RadioButton_CheckedChanged);
        	// 
        	// XYZ_RadioButton
        	// 
        	this.XYZ_RadioButton.AutoSize = true;
        	this.XYZ_RadioButton.Location = new System.Drawing.Point(67, 26);
        	this.XYZ_RadioButton.Name = "XYZ_RadioButton";
        	this.XYZ_RadioButton.Size = new System.Drawing.Size(46, 17);
        	this.XYZ_RadioButton.TabIndex = 2;
        	this.XYZ_RadioButton.Text = "XYZ";
        	this.XYZ_RadioButton.UseVisualStyleBackColor = true;
        	this.XYZ_RadioButton.CheckedChanged += new System.EventHandler(this.RadioButton_CheckedChanged);
        	// 
        	// CMY_RadioButton
        	// 
        	this.CMY_RadioButton.AutoSize = true;
        	this.CMY_RadioButton.Location = new System.Drawing.Point(13, 52);
        	this.CMY_RadioButton.Name = "CMY_RadioButton";
        	this.CMY_RadioButton.Size = new System.Drawing.Size(48, 17);
        	this.CMY_RadioButton.TabIndex = 1;
        	this.CMY_RadioButton.Text = "CMY";
        	this.CMY_RadioButton.UseVisualStyleBackColor = true;
        	this.CMY_RadioButton.CheckedChanged += new System.EventHandler(this.RadioButton_CheckedChanged);
        	// 
        	// RGB_RadioButton
        	// 
        	this.RGB_RadioButton.AutoSize = true;
        	this.RGB_RadioButton.Checked = true;
        	this.RGB_RadioButton.Location = new System.Drawing.Point(13, 26);
        	this.RGB_RadioButton.Name = "RGB_RadioButton";
        	this.RGB_RadioButton.Size = new System.Drawing.Size(48, 17);
        	this.RGB_RadioButton.TabIndex = 0;
        	this.RGB_RadioButton.TabStop = true;
        	this.RGB_RadioButton.Text = "RGB";
        	this.RGB_RadioButton.UseVisualStyleBackColor = true;
        	this.RGB_RadioButton.CheckedChanged += new System.EventHandler(this.RadioButton_CheckedChanged);
        	// 
        	// ColorPlane1Label
        	// 
        	this.ColorPlane1Label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
        	this.ColorPlane1Label.AutoSize = true;
        	this.ColorPlane1Label.BackColor = System.Drawing.SystemColors.ControlDark;
        	this.ColorPlane1Label.Location = new System.Drawing.Point(327, 114);
        	this.ColorPlane1Label.Name = "ColorPlane1Label";
        	this.ColorPlane1Label.Size = new System.Drawing.Size(15, 13);
        	this.ColorPlane1Label.TabIndex = 3;
        	this.ColorPlane1Label.Text = "R";
        	// 
        	// ColorPlane1TrackBar
        	// 
        	this.ColorPlane1TrackBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
        	this.ColorPlane1TrackBar.BackColor = System.Drawing.SystemColors.GrayText;
        	this.ColorPlane1TrackBar.LargeChange = 64;
        	this.ColorPlane1TrackBar.Location = new System.Drawing.Point(343, 108);
        	this.ColorPlane1TrackBar.Maximum = 255;
        	this.ColorPlane1TrackBar.Minimum = -255;
        	this.ColorPlane1TrackBar.Name = "ColorPlane1TrackBar";
        	this.ColorPlane1TrackBar.Size = new System.Drawing.Size(124, 45);
        	this.ColorPlane1TrackBar.TabIndex = 6;
        	this.ColorPlane1TrackBar.TickFrequency = 64;
        	this.ColorPlane1TrackBar.Scroll += new System.EventHandler(this.ColorPlaneTrackBar_Scroll);
        	this.ColorPlane1TrackBar.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ColorPlaneTrackBar_KeyUp);
        	this.ColorPlane1TrackBar.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ColorPlaneTrackBar_MouseUp);
        	// 
        	// ColorPlane2TrackBar
        	// 
        	this.ColorPlane2TrackBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
        	this.ColorPlane2TrackBar.BackColor = System.Drawing.SystemColors.GrayText;
        	this.ColorPlane2TrackBar.LargeChange = 64;
        	this.ColorPlane2TrackBar.Location = new System.Drawing.Point(343, 143);
        	this.ColorPlane2TrackBar.Maximum = 255;
        	this.ColorPlane2TrackBar.Minimum = -255;
        	this.ColorPlane2TrackBar.Name = "ColorPlane2TrackBar";
        	this.ColorPlane2TrackBar.Size = new System.Drawing.Size(124, 45);
        	this.ColorPlane2TrackBar.TabIndex = 8;
        	this.ColorPlane2TrackBar.TickFrequency = 64;
        	this.ColorPlane2TrackBar.Scroll += new System.EventHandler(this.ColorPlaneTrackBar_Scroll);
        	this.ColorPlane2TrackBar.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ColorPlaneTrackBar_KeyUp);
        	this.ColorPlane2TrackBar.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ColorPlaneTrackBar_MouseUp);
        	// 
        	// ColorPlane2Label
        	// 
        	this.ColorPlane2Label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
        	this.ColorPlane2Label.AutoSize = true;
        	this.ColorPlane2Label.BackColor = System.Drawing.SystemColors.ControlDark;
        	this.ColorPlane2Label.Location = new System.Drawing.Point(327, 148);
        	this.ColorPlane2Label.Name = "ColorPlane2Label";
        	this.ColorPlane2Label.Size = new System.Drawing.Size(15, 13);
        	this.ColorPlane2Label.TabIndex = 7;
        	this.ColorPlane2Label.Text = "G";
        	// 
        	// ColorPlane3TrackBar
        	// 
        	this.ColorPlane3TrackBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
        	this.ColorPlane3TrackBar.BackColor = System.Drawing.SystemColors.GrayText;
        	this.ColorPlane3TrackBar.LargeChange = 64;
        	this.ColorPlane3TrackBar.Location = new System.Drawing.Point(343, 178);
        	this.ColorPlane3TrackBar.Maximum = 255;
        	this.ColorPlane3TrackBar.Minimum = -255;
        	this.ColorPlane3TrackBar.Name = "ColorPlane3TrackBar";
        	this.ColorPlane3TrackBar.Size = new System.Drawing.Size(124, 45);
        	this.ColorPlane3TrackBar.TabIndex = 10;
        	this.ColorPlane3TrackBar.TickFrequency = 64;
        	this.ColorPlane3TrackBar.Scroll += new System.EventHandler(this.ColorPlaneTrackBar_Scroll);
        	this.ColorPlane3TrackBar.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ColorPlaneTrackBar_KeyUp);
        	this.ColorPlane3TrackBar.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ColorPlaneTrackBar_MouseUp);
        	// 
        	// ColorPlane3Label
        	// 
        	this.ColorPlane3Label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
        	this.ColorPlane3Label.AutoSize = true;
        	this.ColorPlane3Label.BackColor = System.Drawing.SystemColors.ControlDark;
        	this.ColorPlane3Label.Location = new System.Drawing.Point(327, 184);
        	this.ColorPlane3Label.Name = "ColorPlane3Label";
        	this.ColorPlane3Label.Size = new System.Drawing.Size(14, 13);
        	this.ColorPlane3Label.TabIndex = 11;
        	this.ColorPlane3Label.Text = "B";
        	// 
        	// ColorPlane2TextBox
        	// 
        	this.ColorPlane2TextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
        	this.ColorPlane2TextBox.Location = new System.Drawing.Point(470, 148);
        	this.ColorPlane2TextBox.Name = "ColorPlane2TextBox";
        	this.ColorPlane2TextBox.Size = new System.Drawing.Size(32, 20);
        	this.ColorPlane2TextBox.TabIndex = 9;
        	this.ColorPlane2TextBox.Text = "0";
        	this.ColorPlane2TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        	this.ColorPlane2TextBox.TextChanged += new System.EventHandler(this.TextBox_TextChanged);
        	this.ColorPlane2TextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
        	// 
        	// ColorPlane3TextBox
        	// 
        	this.ColorPlane3TextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
        	this.ColorPlane3TextBox.Location = new System.Drawing.Point(470, 178);
        	this.ColorPlane3TextBox.Name = "ColorPlane3TextBox";
        	this.ColorPlane3TextBox.Size = new System.Drawing.Size(32, 20);
        	this.ColorPlane3TextBox.TabIndex = 11;
        	this.ColorPlane3TextBox.Text = "0";
        	this.ColorPlane3TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        	this.ColorPlane3TextBox.TextChanged += new System.EventHandler(this.TextBox_TextChanged);
        	this.ColorPlane3TextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
        	// 
        	// ColorPlane1TextBox
        	// 
        	this.ColorPlane1TextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
        	this.ColorPlane1TextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
        	this.ColorPlane1TextBox.Location = new System.Drawing.Point(470, 113);
        	this.ColorPlane1TextBox.Name = "ColorPlane1TextBox";
        	this.ColorPlane1TextBox.Size = new System.Drawing.Size(32, 20);
        	this.ColorPlane1TextBox.TabIndex = 7;
        	this.ColorPlane1TextBox.TabStop = false;
        	this.ColorPlane1TextBox.Text = "0";
        	this.ColorPlane1TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        	this.ColorPlane1TextBox.TextChanged += new System.EventHandler(this.TextBox_TextChanged);
        	this.ColorPlane1TextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
        	// 
        	// WaitProgressBar
        	// 
        	this.WaitProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
        	this.WaitProgressBar.Location = new System.Drawing.Point(330, 219);
        	this.WaitProgressBar.Name = "WaitProgressBar";
        	this.WaitProgressBar.Size = new System.Drawing.Size(172, 23);
        	this.WaitProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
        	this.WaitProgressBar.TabIndex = 20;
        	this.WaitProgressBar.Visible = false;
        	// 
        	// panel1
        	// 
        	this.panel1.BackColor = System.Drawing.SystemColors.GrayText;
        	this.panel1.Controls.Add(this.button1);
        	this.panel1.Controls.Add(this.ApplyButton);
        	this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
        	this.panel1.Location = new System.Drawing.Point(0, 0);
        	this.panel1.Name = "panel1";
        	this.panel1.Size = new System.Drawing.Size(525, 301);
        	this.panel1.TabIndex = 21;
        	// 
        	// button1
        	// 
        	this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
        	this.button1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        	this.button1.Location = new System.Drawing.Point(427, 265);
        	this.button1.Name = "button1";
        	this.button1.Size = new System.Drawing.Size(75, 23);
        	this.button1.TabIndex = 1;
        	this.button1.Text = "Отмена";
        	this.button1.UseVisualStyleBackColor = true;
        	this.button1.Click += new System.EventHandler(this.CanselButton_Click);
        	// 
        	// ApplyButton
        	// 
        	this.ApplyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
        	this.ApplyButton.DialogResult = System.Windows.Forms.DialogResult.OK;
        	this.ApplyButton.Location = new System.Drawing.Point(330, 265);
        	this.ApplyButton.Name = "ApplyButton";
        	this.ApplyButton.Size = new System.Drawing.Size(91, 23);
        	this.ApplyButton.TabIndex = 0;
        	this.ApplyButton.Text = "Применить";
        	this.ApplyButton.UseVisualStyleBackColor = true;
        	this.ApplyButton.Click += new System.EventHandler(this.ApplyButton_Click);
        	// 
        	// ImagePanel
        	// 
        	this.ImagePanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
        	        	        	| System.Windows.Forms.AnchorStyles.Left) 
        	        	        	| System.Windows.Forms.AnchorStyles.Right)));
        	this.ImagePanel.BackColor = System.Drawing.Color.White;
        	this.ImagePanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
        	this.ImagePanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        	this.ImagePanel.Image = null;
        	this.ImagePanel.Location = new System.Drawing.Point(12, 12);
        	this.ImagePanel.Name = "ImagePanel";
        	this.ImagePanel.Size = new System.Drawing.Size(293, 276);
        	this.ImagePanel.TabIndex = 2;
        	this.ImagePanel.Paint += new System.Windows.Forms.PaintEventHandler(this.ImagePanel_Paint);
        	// 
        	// ColorSchemsForm
        	// 
        	this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        	this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        	this.BackColor = System.Drawing.SystemColors.ControlDarkDark;
        	this.ClientSize = new System.Drawing.Size(525, 301);
        	this.Controls.Add(this.WaitProgressBar);
        	this.Controls.Add(this.ColorPlane1TextBox);
        	this.Controls.Add(this.ColorPlane3TextBox);
        	this.Controls.Add(this.ColorPlane2TextBox);
        	this.Controls.Add(this.ColorPlane3TrackBar);
        	this.Controls.Add(this.ColorPlane3Label);
        	this.Controls.Add(this.ColorPlane2TrackBar);
        	this.Controls.Add(this.ColorPlane2Label);
        	this.Controls.Add(this.ColorPlane1TrackBar);
        	this.Controls.Add(this.ColorPlane1Label);
        	this.Controls.Add(this.ImagePanel);
        	this.Controls.Add(this.groupBox1);
        	this.Controls.Add(this.panel1);
        	this.MinimumSize = new System.Drawing.Size(533, 335);
        	this.Name = "ColorSchemsForm";
        	this.Text = "Цветовые схемы";
        	this.Shown += new System.EventHandler(this.ColorSchemsForm_Shown);
        	this.groupBox1.ResumeLayout(false);
        	this.groupBox1.PerformLayout();
        	((System.ComponentModel.ISupportInitialize)(this.ColorPlane1TrackBar)).EndInit();
        	((System.ComponentModel.ISupportInitialize)(this.ColorPlane2TrackBar)).EndInit();
        	((System.ComponentModel.ISupportInitialize)(this.ColorPlane3TrackBar)).EndInit();
        	this.panel1.ResumeLayout(false);
        	this.ResumeLayout(false);
        	this.PerformLayout();
        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private WinControls.PanelEx ImagePanel;
        private System.Windows.Forms.RadioButton RGB_RadioButton;
        private System.Windows.Forms.RadioButton XYZ_RadioButton;
        private System.Windows.Forms.RadioButton CMY_RadioButton;
        private System.Windows.Forms.RadioButton Lab_RadioButton;
        private System.Windows.Forms.RadioButton YUV_RadioButton;
        private System.Windows.Forms.RadioButton HSV_RadioButton;
        private System.Windows.Forms.Label ColorPlane1Label;
        private System.Windows.Forms.TrackBar ColorPlane1TrackBar;
        private System.Windows.Forms.TrackBar ColorPlane2TrackBar;
        private System.Windows.Forms.Label ColorPlane2Label;
        private System.Windows.Forms.TrackBar ColorPlane3TrackBar;
        private System.Windows.Forms.Label ColorPlane3Label;
        private System.Windows.Forms.TextBox ColorPlane2TextBox;
        private System.Windows.Forms.TextBox ColorPlane3TextBox;
        private System.Windows.Forms.TextBox ColorPlane1TextBox;
        private System.Windows.Forms.ProgressBar WaitProgressBar;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button ApplyButton;
        private System.Windows.Forms.Button button1;
    }
}