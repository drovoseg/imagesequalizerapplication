﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

using ImagesEqualizerApplication.Utils;
using ImgModLib;
using ImgModLib.Graphic;

namespace ImagesEqualizerApplication
{
    public partial class ColorSchemsForm : Form
    {        
        private struct AddParams
        {
            public ImageEx Image;
            public string PlaneName;
            public double Value;

            public AddParams(ImageEx image, string planename, double value)
            {
                Image = image;
                PlaneName = planename;
                Value = value;
            }
        }

        ImageEx image;        
        int Plane1_OldVal = 0, Plane2_OldVal = 0, Plane3_OldVal = 0;
        Point location; Size size;
        bool needWait = false;

        public ImageEx Image
        {
            get { return image; }
        }

        public ColorSchemsForm(ImageEx img, EqualizerForm form)
        {
            InitializeComponent();
            Icon = Properties.Resources.EqualizerIcon;

            location = form.Location;
            size = form.Size;
            
            ImagePanel.Image = (Image) img;
            image = img;
            ColorPlane1TrackBar.Name = "R";
            ColorPlane2TrackBar.Name = "G";
            ColorPlane3TrackBar.Name = "B";
        }
        
        void DrawImageOnPanel(bool NeedScaling)
        {
            if (image.Height == ImagePanel.Height && image.Width == ImagePanel.Width)
            {
                image.Draw(0, 0, ImagePanel.Width, ImagePanel.Height,
                           Graphics.FromHwnd(ImagePanel.Handle));
            }
            else if (image.Height < ImagePanel.Height && image.Width < ImagePanel.Width)
            {
                Bitmap bufimg = new Bitmap(ImagePanel.Width, ImagePanel.Height);
                Graphics g = Graphics.FromImage(bufimg);

                int drawY = (ImagePanel.Height - image.Height) / 2;
                int drawX = (ImagePanel.Width - image.Width) / 2;

                g.FillRectangle(new SolidBrush(ImagePanel.BackColor),
                                0, 0, ImagePanel.Width, ImagePanel.Height);

                image.Draw(drawX, drawY, image.Width, image.Height, g);

                Graphics.FromHwnd(ImagePanel.Handle).DrawImageUnscaled(bufimg, 0, 0);

                bufimg.Dispose();
                bufimg = null;
                g.Dispose();
                g = null;
            }
            else if (NeedScaling)
            {
                Bitmap bufimg = new Bitmap(ImagePanel.Width, ImagePanel.Height);
                Graphics g = Graphics.FromImage(bufimg);
                SolidBrush brush = new SolidBrush(ImagePanel.BackColor);
                double k, kx, ky;

                kx = ImagePanel.Width / (double)image.Width;
                ky = ImagePanel.Height / (double)image.Height;

                if (ky < kx)
                {
                    k = ky;

                    int drawWidth = (int)(image.Width * k);
                    int drawHeigth = (int)(image.Height * k);

                    int drawX = (ImagePanel.Width - drawWidth) / 2;

                    g.FillRectangle(brush, 0, 0, drawX, bufimg.Height);
                    image.Draw(drawX, 0, drawWidth, drawHeigth, g);
                    g.FillRectangle(brush, bufimg.Width - drawX - 1, 0, drawX, bufimg.Height);
                }
                else
                {
                    k = kx;

                    int drawWidth = (int)(image.Width * k);
                    int drawHeigth = (int)(image.Height * k);

                    int drawY = (ImagePanel.Height - drawHeigth) / 2;

                    g.FillRectangle(brush, 0, 0, bufimg.Width, drawY);
                    image.Draw(0, drawY, drawWidth, drawHeigth, g);
                    g.FillRectangle(brush, 0, bufimg.Height - drawY - 1, bufimg.Width, drawY);
                }

                Graphics.FromHwnd(ImagePanel.Handle).DrawImageUnscaled(bufimg, 0, 0);

                bufimg.Dispose();
                bufimg = null;
                g.Dispose();
                g = null;
            }
            else
            {
                image.Draw(-ImagePanel.AutoScrollPosition.X, -ImagePanel.AutoScrollPosition.Y,
                           ImagePanel.Width, ImagePanel.Height, 0, 0, ImagePanel.Width, ImagePanel.Height,
                           Graphics.FromHwnd(ImagePanel.Handle));
            }

            GC.Collect();

        }

        private void ImagePanel_Paint(object sender, PaintEventArgs e)
        {
            DrawImageOnPanel(true);            
        }

        void AddValueToPlane(Object Parametrs)
        {
            needWait = true;

            ImageEx image = ((AddParams)Parametrs).Image;
            string name = ((AddParams)Parametrs).PlaneName;
            double val = ((AddParams)Parametrs).Value;

            image.Planes[name] = image.Planes[name].Add(val);
            image.Update();

            needWait = false;

        }

        private void ColorPlaneTrackBar_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Left && e.KeyCode != Keys.Right
                && e.KeyCode != Keys.PageUp && e.KeyCode != Keys.PageDown
                && e.KeyCode != Keys.End && e.KeyCode != Keys.Home
                && e.KeyCode != Keys.Up)
                return;

            TrackBar SenderTrackBar = (TrackBar)sender;
            string name = SenderTrackBar.Name;
            double val = 0;
            Thread thread;

            switch (e.KeyCode)
            {
                case Keys.Left: case Keys.Right: case Keys.Home: case Keys.End:
                    if (SenderTrackBar == ColorPlane1TrackBar)
                        val = SenderTrackBar.Value - Plane1_OldVal;                    
                    else if (SenderTrackBar == ColorPlane2TrackBar)
                        val = SenderTrackBar.Value - Plane2_OldVal;
                    else if (SenderTrackBar == ColorPlane3TrackBar)
                        val = SenderTrackBar.Value - Plane3_OldVal;
                    break;

                case Keys.PageUp: case Keys.PageDown:
                    val = SenderTrackBar.LargeChange;
                    break;
            }

            if (val == 0) return;

            thread = new Thread(new ParameterizedThreadStart(AddValueToPlane));
            thread.IsBackground = true;

            switch (e.KeyCode)
            {
                case Keys.Left:
                    if (SenderTrackBar.Value > SenderTrackBar.Minimum)
                    {
                        thread.Start(new AddParams(image, name, val / (Lab_RadioButton.Checked ? 1 : ImageEx.NORMA)));
                    }
                    break;

                case Keys.Right:
                    if (SenderTrackBar.Value < SenderTrackBar.Maximum)
                    {
                        thread.Start(new AddParams(image, name, val / (Lab_RadioButton.Checked ? 1 : ImageEx.NORMA)));
                    }
                    break;

                case Keys.PageUp:
                    if (SenderTrackBar.Value == SenderTrackBar.Maximum)
                        return;

                    thread.Start(new AddParams(image, name, -val / (Lab_RadioButton.Checked ? 1 : ImageEx.NORMA)));
                    break;

                case Keys.PageDown:
                    if (SenderTrackBar.Value == SenderTrackBar.Minimum)
                        return;

                    thread.Start(new AddParams(image, name, val / (Lab_RadioButton.Checked ? 1 : ImageEx.NORMA)));
                    break;

                case Keys.Home: case Keys.End:
                    thread.Start(new AddParams(image, name, val / (Lab_RadioButton.Checked ? 1 : ImageEx.NORMA)));
                    break;
            }
            Thread.Sleep(Util.DELAY_BEFORE_PROGRESS_SHOW_MS);            

            ActivateProgressBar();
            DrawImageOnPanel(true);

            if (SenderTrackBar == ColorPlane1TrackBar)
                Plane1_OldVal = SenderTrackBar.Value;
            else if (SenderTrackBar == ColorPlane2TrackBar)
                Plane2_OldVal = SenderTrackBar.Value;
            else if (SenderTrackBar == ColorPlane3TrackBar)
                Plane3_OldVal = SenderTrackBar.Value;

            thread = null;
            GC.Collect();
            
        }

        private void ActivateProgressBar()
        {            
            Cursor = Cursors.WaitCursor;
            
            WaitProgressBar.Value = 0;
            WaitProgressBar.Visible = true;

            WaitProgressBar.Invalidate();
            WaitProgressBar.Update();

            while (needWait)
            {
                WaitProgressBar.Invalidate();
                WaitProgressBar.Update();
                Thread.Sleep(Util.DELAY_BEFORE_PROGRESS_SHOW_MS);
            }

            WaitProgressBar.Visible = false;

            Cursor = Cursors.Default;
        }

        private void ColorPlaneTrackBar_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Middle || e.Button == MouseButtons.Right)
                return;

            TrackBar SenderTrackBar = (TrackBar)sender;
            Thread thread;
            string name = SenderTrackBar.Name;
            double val = 0;

            if (SenderTrackBar == ColorPlane1TrackBar)
                val = SenderTrackBar.Value - Plane1_OldVal;
            else if (SenderTrackBar == ColorPlane2TrackBar)
                val = SenderTrackBar.Value - Plane2_OldVal;
            else if (SenderTrackBar == ColorPlane3TrackBar)
                val = SenderTrackBar.Value - Plane3_OldVal;

            if (val == 0) return;

            thread = new Thread(new ParameterizedThreadStart(AddValueToPlane));
            thread.IsBackground = true;
            thread.Start(new AddParams(image, name, val / (Lab_RadioButton.Checked ? 1 : ImageEx.NORMA)));
            Thread.Sleep(Util.DELAY_BEFORE_PROGRESS_SHOW_MS);

            ActivateProgressBar();
            DrawImageOnPanel(true);

            if (SenderTrackBar == ColorPlane1TrackBar)
                Plane1_OldVal = SenderTrackBar.Value;
            else if (SenderTrackBar == ColorPlane2TrackBar)
                Plane2_OldVal = SenderTrackBar.Value;
            else if (SenderTrackBar == ColorPlane3TrackBar)
                Plane3_OldVal = SenderTrackBar.Value;

            thread = null;
            GC.Collect();
        }

        private void ColorPlaneTrackBar_Scroll(object sender, EventArgs e)
        {
            TrackBar SenderTrackBar = (TrackBar)sender;

            if (SenderTrackBar == ColorPlane1TrackBar)
                ColorPlane1TextBox.Text = SenderTrackBar.Value.ToString();
            else if (SenderTrackBar == ColorPlane2TrackBar)
                ColorPlane2TextBox.Text = SenderTrackBar.Value.ToString();
            else
                ColorPlane3TextBox.Text = SenderTrackBar.Value.ToString();
        }

        void PrepareImage(Object obj)
        {
            needWait = true;
            
            ImageEx img = (ImageEx)obj;
            image.ToRGB();
            image.Eclipse();
            image.Update();

            needWait = false;
        }

        private void ApplyButton_Click(object sender, EventArgs e)
        {
            Thread thread = new Thread(new ParameterizedThreadStart(PrepareImage));           

            thread.IsBackground = true;
            thread.Start(image);
            Thread.Sleep(Util.DELAY_BEFORE_PROGRESS_SHOW_MS);
            ActivateProgressBar();

            Close();
        }

        private void CanselButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void TextBox_TextChanged(object sender, EventArgs e)
        {
            int rez;
            TextBox SelTextBox = (TextBox)sender;
            string str = SelTextBox.Text;
                        
            if (!int.TryParse(str, out rez) && str.Length > 0)
            {
                int index = SelTextBox.SelectionStart - 1;
                SelTextBox.Text = str.Remove(index, 1);
                SelTextBox.SelectionStart++;
            }
        }

        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            TextBox SelTextbox = (TextBox)sender;
            TrackBar SelTrackBar;
            Thread thread;
            double val;
            string name;

            if (SelTextbox.Text == "") return;
            if (e.KeyData != Keys.Enter) return;

            if (SelTextbox == ColorPlane1TextBox)
                SelTrackBar = ColorPlane1TrackBar;
            else if (SelTextbox == ColorPlane2TextBox)
                SelTrackBar = ColorPlane2TrackBar;
            else
                SelTrackBar = ColorPlane3TrackBar;

            name = SelTrackBar.Name;

            if (int.Parse(SelTextbox.Text) < SelTrackBar.Minimum)
                SelTextbox.Text = SelTrackBar.Minimum.ToString();
            if (int.Parse(SelTextbox.Text) > SelTrackBar.Maximum)
                SelTextbox.Text = SelTrackBar.Maximum.ToString();


            val = int.Parse(SelTextbox.Text) - SelTrackBar.Value;
            if (val == 0) return;

            thread = new Thread(new ParameterizedThreadStart(AddValueToPlane));
            thread.IsBackground = true;
            thread.Start(new AddParams(image, name, val / (Lab_RadioButton.Checked ? 1 : ImageEx.NORMA)));
            Thread.Sleep(Util.DELAY_BEFORE_PROGRESS_SHOW_MS);

            ActivateProgressBar();
            SelTrackBar.Value = int.Parse(SelTextbox.Text);
            DrawImageOnPanel(true);

            if (SelTrackBar == ColorPlane1TrackBar)
                Plane1_OldVal = SelTrackBar.Value;
            else if (SelTrackBar == ColorPlane2TrackBar)
                Plane2_OldVal = SelTrackBar.Value;
            else if (SelTrackBar == ColorPlane3TrackBar)
                Plane3_OldVal = SelTrackBar.Value;

            thread = null;
            GC.Collect();
        }

        private void RadioButton_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton SelRadioButton = (RadioButton)sender;
            Thread thread;
            ColorSchema NewSchema = ColorSchema.RGB;

            if (!SelRadioButton.Checked) return;

            DateTime t1 = DateTime.Now;
            thread = new Thread(new ParameterizedThreadStart(ChangeSchema));
            thread.IsBackground = true;

            ColorPlane1TrackBar.Value = ColorPlane2TrackBar.Value = ColorPlane3TrackBar.Value = 0;            
            ColorPlane1TextBox.Text = ColorPlane2TextBox.Text = ColorPlane3TextBox.Text = "0";

            ColorPlane1Label.Enabled = ColorPlane1TrackBar.Enabled = ColorPlane1TextBox.Enabled = true;
            ColorPlane2Label.Enabled = ColorPlane2TrackBar.Enabled = ColorPlane2TextBox.Enabled = true;
            ColorPlane3Label.Enabled = ColorPlane3TrackBar.Enabled = ColorPlane3TextBox.Enabled = true;

            if (SelRadioButton == RGB_RadioButton)
            {
                ColorPlane1Label.Text = ColorPlane1TrackBar.Name = "R";
                ColorPlane2Label.Text = ColorPlane2TrackBar.Name = "G";
                ColorPlane3Label.Text = ColorPlane3TrackBar.Name = "B";

                NewSchema = ColorSchema.RGB;
            }
            else if (SelRadioButton == CMY_RadioButton)
            {
                ColorPlane1Label.Text = ColorPlane1TrackBar.Name = "C";
                ColorPlane2Label.Text = ColorPlane2TrackBar.Name = "M";
                ColorPlane3Label.Text = ColorPlane3TrackBar.Name = "Y";

                NewSchema = ColorSchema.CMY;
            }
            else if (SelRadioButton == XYZ_RadioButton)
            {
                ColorPlane1Label.Text = ColorPlane1TrackBar.Name = "X";
                ColorPlane2Label.Text = ColorPlane2TrackBar.Name = "Y";
                ColorPlane3Label.Text = ColorPlane3TrackBar.Name = "Z";

                NewSchema = ColorSchema.XYZ;
            }
            else if (SelRadioButton == HSV_RadioButton)
            {
                ColorPlane1Label.Text = ColorPlane1TrackBar.Name = "H";
                ColorPlane2Label.Text = ColorPlane2TrackBar.Name = "S";
                ColorPlane3Label.Text = ColorPlane3TrackBar.Name = "V";

                NewSchema = ColorSchema.HSV;
            }
            else if (SelRadioButton == YUV_RadioButton)
            {
                ColorPlane1Label.Text = ColorPlane1TrackBar.Name = "Y";
                ColorPlane2Label.Text = ColorPlane2TrackBar.Name = "U";
                ColorPlane3Label.Text = ColorPlane3TrackBar.Name = "V";

                NewSchema = ColorSchema.YUV;
            }
            else if (SelRadioButton == Lab_RadioButton)
            {
                ColorPlane1Label.Text = ColorPlane1TrackBar.Name = "L";
                ColorPlane2Label.Text = ColorPlane2TrackBar.Name = "a";
                ColorPlane3Label.Text = ColorPlane3TrackBar.Name = "b";

                NewSchema = ColorSchema.Lab;
            }

            thread.Start(NewSchema);
            Thread.Sleep(Util.DELAY_BEFORE_PROGRESS_SHOW_MS);

            ActivateProgressBar();

            SetTrackBarBorderVals(ColorPlane1TrackBar, image.Planes[1]);
            SetTrackBarBorderVals(ColorPlane2TrackBar, image.Planes[2]);
            SetTrackBarBorderVals(ColorPlane3TrackBar, image.Planes[3]);
            Plane1_OldVal = Plane2_OldVal = Plane3_OldVal = 0;

            thread = null;
            GC.Collect();
        }

        void ChangeSchema(Object schema)
        {
            needWait = true;

            switch ((ColorSchema)schema)
            {
                case ColorSchema.RGB:
                    image.ToRGB();
                    break;

                case ColorSchema.CMY:
                    image.ToCMY();
                    break;

                case ColorSchema.XYZ:
                    image.ToXYZ();
                    break;

                case ColorSchema.YUV:
                    image.ToYUV();
                    break;

                case ColorSchema.Lab:
                    image.ToLab();
                    break;

                case ColorSchema.HSV:
                    image.ToHSV();
                    break;
            }
                        
            needWait = false;
        }

        void SetTrackBarBorderVals(TrackBar trackbar, ColorPlane plane)
        {
            trackbar.Maximum = (int)Math.Floor(plane.MaxValue * 
                (Lab_RadioButton.Checked ? 1 : ImageEx.NORMA));
            
            trackbar.Minimum = -trackbar.Maximum;            
        }

        private void ColorSchemsForm_Shown(object sender, EventArgs e)
        {
            Location = location;
            Size = size;
        }
    }
}
