﻿namespace ImagesEqualizerApplication
{
    partial class EqualizerForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
        	this.LoadImageBkgWorker = new System.ComponentModel.BackgroundWorker();
        	this.menuStrip1 = new System.Windows.Forms.MenuStrip();
        	this.FileMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.OpenMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.SaveMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.SaveAsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.imageMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.ColorSchemasMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.ChangeSizeMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.помощьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.AboutMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.ScaleCheckBox = new System.Windows.Forms.CheckBox();
        	this.panel1 = new System.Windows.Forms.Panel();
        	this.ImagePanel = new WinControls.PanelEx();
        	this.WaitProgressBar = new System.Windows.Forms.ProgressBar();
        	this.panel2 = new System.Windows.Forms.Panel();
        	this.panel3 = new System.Windows.Forms.Panel();
        	this.GistogramsTableLayout = new System.Windows.Forms.TableLayoutPanel();
        	this.GGistogramPanel = new WinControls.PanelEx();
        	this.RGistogramPanel = new WinControls.PanelEx();
        	this.BGistogramPanel = new WinControls.PanelEx();
        	this.ApplyButton = new System.Windows.Forms.Button();
        	this.panel4 = new System.Windows.Forms.Panel();
        	this.panel5 = new System.Windows.Forms.Panel();
        	this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
        	this.panel9 = new System.Windows.Forms.Panel();
        	this.ParametrLabel = new System.Windows.Forms.Label();
        	this.AppertureLabel = new System.Windows.Forms.Label();
        	this.AppertureComboBox = new System.Windows.Forms.ComboBox();
        	this.FilterLabel = new System.Windows.Forms.Label();
        	this.ParamUpDown = new System.Windows.Forms.NumericUpDown();
        	this.FilterComboBox = new System.Windows.Forms.ComboBox();
        	this.FilterGroupBox = new System.Windows.Forms.GroupBox();
        	this.BFilterCheckBox = new System.Windows.Forms.CheckBox();
        	this.GFilterCheckBox = new System.Windows.Forms.CheckBox();
        	this.RFilterCheckBox = new System.Windows.Forms.CheckBox();
        	this.SizeUpDown = new System.Windows.Forms.NumericUpDown();
        	this.SizeLabel = new System.Windows.Forms.Label();
        	this.panel8 = new System.Windows.Forms.Panel();
        	this.GradTransfLabel = new System.Windows.Forms.Label();
        	this.GradTransfComboBox = new System.Windows.Forms.ComboBox();
        	this.GradTransfGroupBox = new System.Windows.Forms.GroupBox();
        	this.BGradCheckBox = new System.Windows.Forms.CheckBox();
        	this.GGradCheckBox = new System.Windows.Forms.CheckBox();
        	this.RGradCheckBox = new System.Windows.Forms.CheckBox();
        	this.panel7 = new System.Windows.Forms.Panel();
        	this.MultiplierUpDown = new System.Windows.Forms.NumericUpDown();
        	this.OperationsLabel = new System.Windows.Forms.Label();
        	this.OperationsComboBox = new System.Windows.Forms.ComboBox();
        	this.FirstGroupBox = new System.Windows.Forms.GroupBox();
        	this.FirstB_RadioBut = new System.Windows.Forms.RadioButton();
        	this.FirstG_RadioBut = new System.Windows.Forms.RadioButton();
        	this.FirstR_RadioBut = new System.Windows.Forms.RadioButton();
        	this.SecondGroupBox = new System.Windows.Forms.GroupBox();
        	this.SecondB_RadioBut = new System.Windows.Forms.RadioButton();
        	this.SecondG_RadioBut = new System.Windows.Forms.RadioButton();
        	this.SecondR_RadioBut = new System.Windows.Forms.RadioButton();
        	this.OperationSign = new System.Windows.Forms.Label();
        	this.panel6 = new System.Windows.Forms.Panel();
        	this.BackgroundsPanel = new System.Windows.Forms.Panel();
        	this.pictureLabel5 = new WinControls.PictureLabel();
        	this.pictureLabel4 = new WinControls.PictureLabel();
        	this.pictureLabel3 = new WinControls.PictureLabel();
        	this.pictureLabel2 = new WinControls.PictureLabel();
        	this.pictureLabel1 = new WinControls.PictureLabel();
        	this.BackgroundsLabel = new System.Windows.Forms.Label();
        	this.menuStrip1.SuspendLayout();
        	this.panel1.SuspendLayout();
        	this.ImagePanel.SuspendLayout();
        	this.panel2.SuspendLayout();
        	this.panel3.SuspendLayout();
        	this.GistogramsTableLayout.SuspendLayout();
        	this.panel4.SuspendLayout();
        	this.panel5.SuspendLayout();
        	this.tableLayoutPanel1.SuspendLayout();
        	this.panel9.SuspendLayout();
        	((System.ComponentModel.ISupportInitialize)(this.ParamUpDown)).BeginInit();
        	this.FilterGroupBox.SuspendLayout();
        	((System.ComponentModel.ISupportInitialize)(this.SizeUpDown)).BeginInit();
        	this.panel8.SuspendLayout();
        	this.GradTransfGroupBox.SuspendLayout();
        	this.panel7.SuspendLayout();
        	((System.ComponentModel.ISupportInitialize)(this.MultiplierUpDown)).BeginInit();
        	this.FirstGroupBox.SuspendLayout();
        	this.SecondGroupBox.SuspendLayout();
        	this.panel6.SuspendLayout();
        	this.BackgroundsPanel.SuspendLayout();
        	this.SuspendLayout();
        	// 
        	// LoadImageBkgWorker
        	// 
        	this.LoadImageBkgWorker.WorkerReportsProgress = true;
        	this.LoadImageBkgWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.LoadImageInBackground);
        	this.LoadImageBkgWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.BackgroungImageLoadingIsComplete);
        	// 
        	// menuStrip1
        	// 
        	this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
        	        	        	this.FileMenuItem,
        	        	        	this.imageMenuItem,
        	        	        	this.помощьToolStripMenuItem});
        	this.menuStrip1.Location = new System.Drawing.Point(0, 0);
        	this.menuStrip1.Name = "menuStrip1";
        	this.menuStrip1.Size = new System.Drawing.Size(798, 24);
        	this.menuStrip1.TabIndex = 0;
        	this.menuStrip1.Text = "menuStrip1";
        	// 
        	// FileMenuItem
        	// 
        	this.FileMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
        	        	        	this.OpenMenuItem,
        	        	        	this.SaveMenuItem,
        	        	        	this.SaveAsMenuItem});
        	this.FileMenuItem.Name = "FileMenuItem";
        	this.FileMenuItem.Size = new System.Drawing.Size(45, 20);
        	this.FileMenuItem.Text = "&Файл";
        	// 
        	// OpenMenuItem
        	// 
        	this.OpenMenuItem.Name = "OpenMenuItem";
        	this.OpenMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
        	this.OpenMenuItem.Size = new System.Drawing.Size(241, 22);
        	this.OpenMenuItem.Text = "&Открыть";
        	this.OpenMenuItem.Click += new System.EventHandler(this.OpenMenuItem_Click);
        	// 
        	// SaveMenuItem
        	// 
        	this.SaveMenuItem.Enabled = false;
        	this.SaveMenuItem.Name = "SaveMenuItem";
        	this.SaveMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
        	this.SaveMenuItem.Size = new System.Drawing.Size(241, 22);
        	this.SaveMenuItem.Text = "&Сохранить";
        	this.SaveMenuItem.Click += new System.EventHandler(this.SaveMenuItem_Click);
        	// 
        	// SaveAsMenuItem
        	// 
        	this.SaveAsMenuItem.Enabled = false;
        	this.SaveAsMenuItem.Name = "SaveAsMenuItem";
        	this.SaveAsMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
        	        	        	| System.Windows.Forms.Keys.S)));
        	this.SaveAsMenuItem.Size = new System.Drawing.Size(241, 22);
        	this.SaveAsMenuItem.Text = "Сохранить &как...";
        	this.SaveAsMenuItem.Click += new System.EventHandler(this.SaveAsMenuItem_Click);
        	// 
        	// imageMenuItem
        	// 
        	this.imageMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
        	        	        	this.ColorSchemasMenuItem,
        	        	        	this.ChangeSizeMenuItem});
        	this.imageMenuItem.Enabled = false;
        	this.imageMenuItem.Name = "imageMenuItem";
        	this.imageMenuItem.Size = new System.Drawing.Size(87, 20);
        	this.imageMenuItem.Text = "&Изображение";
        	// 
        	// ColorSchemasMenuItem
        	// 
        	this.ColorSchemasMenuItem.Name = "ColorSchemasMenuItem";
        	this.ColorSchemasMenuItem.Size = new System.Drawing.Size(183, 22);
        	this.ColorSchemasMenuItem.Text = "&Цветовые схемы";
        	this.ColorSchemasMenuItem.Click += new System.EventHandler(this.ColorSchemasMenuItem_Click);
        	// 
        	// ChangeSizeMenuItem
        	// 
        	this.ChangeSizeMenuItem.Name = "ChangeSizeMenuItem";
        	this.ChangeSizeMenuItem.Size = new System.Drawing.Size(183, 22);
        	this.ChangeSizeMenuItem.Text = "&Изменение размера";
        	this.ChangeSizeMenuItem.Visible = false;
        	// 
        	// помощьToolStripMenuItem
        	// 
        	this.помощьToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
        	        	        	this.AboutMenuItem});
        	this.помощьToolStripMenuItem.Name = "помощьToolStripMenuItem";
        	this.помощьToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
        	this.помощьToolStripMenuItem.Text = "&Помощь ";
        	// 
        	// AboutMenuItem
        	// 
        	this.AboutMenuItem.Name = "AboutMenuItem";
        	this.AboutMenuItem.Size = new System.Drawing.Size(161, 22);
        	this.AboutMenuItem.Text = "О &программе...";
        	this.AboutMenuItem.Click += new System.EventHandler(this.AboutMenuItem_Click);
        	// 
        	// ScaleCheckBox
        	// 
        	this.ScaleCheckBox.AutoSize = true;
        	this.ScaleCheckBox.Enabled = false;
        	this.ScaleCheckBox.Location = new System.Drawing.Point(15, 10);
        	this.ScaleCheckBox.Name = "ScaleCheckBox";
        	this.ScaleCheckBox.Size = new System.Drawing.Size(184, 17);
        	this.ScaleCheckBox.TabIndex = 0;
        	this.ScaleCheckBox.Text = "Масштабировать изображение";
        	this.ScaleCheckBox.UseVisualStyleBackColor = true;
        	this.ScaleCheckBox.CheckedChanged += new System.EventHandler(this.ScaleCheckBox_CheckedChanged);
        	// 
        	// panel1
        	// 
        	this.panel1.BackColor = System.Drawing.SystemColors.GrayText;
        	this.panel1.Controls.Add(this.ImagePanel);
        	this.panel1.Controls.Add(this.ScaleCheckBox);
        	this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
        	this.panel1.Location = new System.Drawing.Point(0, 0);
        	this.panel1.Name = "panel1";
        	this.panel1.Size = new System.Drawing.Size(495, 309);
        	this.panel1.TabIndex = 4;
        	// 
        	// ImagePanel
        	// 
        	this.ImagePanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
        	        	        	| System.Windows.Forms.AnchorStyles.Left) 
        	        	        	| System.Windows.Forms.AnchorStyles.Right)));
        	this.ImagePanel.BackColor = System.Drawing.Color.White;
        	this.ImagePanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
        	this.ImagePanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        	this.ImagePanel.Controls.Add(this.WaitProgressBar);
        	this.ImagePanel.Image = null;
        	this.ImagePanel.Location = new System.Drawing.Point(12, 34);
        	this.ImagePanel.Name = "ImagePanel";
        	this.ImagePanel.Size = new System.Drawing.Size(483, 275);
        	this.ImagePanel.TabIndex = 1;
        	this.ImagePanel.Scroll += new System.Windows.Forms.ScrollEventHandler(this.ImagePanel_Scroll);
        	this.ImagePanel.Paint += new System.Windows.Forms.PaintEventHandler(this.ImagePanel_Paint);
        	this.ImagePanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ImagePanel_MouseDown);
        	this.ImagePanel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ImagePanel_MouseMove);
        	this.ImagePanel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ImagePanel_MouseUp);
        	this.ImagePanel.Resize += new System.EventHandler(this.ImagePanel_Resize);
        	// 
        	// WaitProgressBar
        	// 
        	this.WaitProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
        	this.WaitProgressBar.Location = new System.Drawing.Point(146, 106);
        	this.WaitProgressBar.Name = "WaitProgressBar";
        	this.WaitProgressBar.Size = new System.Drawing.Size(173, 31);
        	this.WaitProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
        	this.WaitProgressBar.TabIndex = 0;
        	this.WaitProgressBar.Visible = false;
        	// 
        	// panel2
        	// 
        	this.panel2.Controls.Add(this.panel1);
        	this.panel2.Controls.Add(this.panel3);
        	this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
        	this.panel2.Location = new System.Drawing.Point(0, 24);
        	this.panel2.MinimumSize = new System.Drawing.Size(1, 304);
        	this.panel2.Name = "panel2";
        	this.panel2.Size = new System.Drawing.Size(798, 309);
        	this.panel2.TabIndex = 5;
        	// 
        	// panel3
        	// 
        	this.panel3.BackColor = System.Drawing.SystemColors.GrayText;
        	this.panel3.Controls.Add(this.GistogramsTableLayout);
        	this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
        	this.panel3.Location = new System.Drawing.Point(495, 0);
        	this.panel3.Name = "panel3";
        	this.panel3.Size = new System.Drawing.Size(303, 309);
        	this.panel3.TabIndex = 5;
        	// 
        	// GistogramsTableLayout
        	// 
        	this.GistogramsTableLayout.ColumnCount = 1;
        	this.GistogramsTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
        	this.GistogramsTableLayout.Controls.Add(this.GGistogramPanel, 0, 1);
        	this.GistogramsTableLayout.Controls.Add(this.RGistogramPanel, 0, 0);
        	this.GistogramsTableLayout.Controls.Add(this.BGistogramPanel, 0, 2);
        	this.GistogramsTableLayout.Dock = System.Windows.Forms.DockStyle.Fill;
        	this.GistogramsTableLayout.Location = new System.Drawing.Point(0, 0);
        	this.GistogramsTableLayout.Name = "GistogramsTableLayout";
        	this.GistogramsTableLayout.Padding = new System.Windows.Forms.Padding(5);
        	this.GistogramsTableLayout.RowCount = 3;
        	this.GistogramsTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
        	this.GistogramsTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
        	this.GistogramsTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
        	this.GistogramsTableLayout.Size = new System.Drawing.Size(303, 309);
        	this.GistogramsTableLayout.TabIndex = 0;
        	this.GistogramsTableLayout.Paint += new System.Windows.Forms.PaintEventHandler(this.GistogramsTableLayout_Paint);
        	this.GistogramsTableLayout.Resize += new System.EventHandler(this.GistogramsTableLayout_Resize);
        	// 
        	// GGistogramPanel
        	// 
        	this.GGistogramPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
        	        	        	| System.Windows.Forms.AnchorStyles.Left) 
        	        	        	| System.Windows.Forms.AnchorStyles.Right)));
        	this.GGistogramPanel.BackColor = System.Drawing.Color.Black;
        	this.GGistogramPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
        	this.GGistogramPanel.Image = null;
        	this.GGistogramPanel.Location = new System.Drawing.Point(8, 107);
        	this.GGistogramPanel.Name = "GGistogramPanel";
        	this.GGistogramPanel.Size = new System.Drawing.Size(287, 93);
        	this.GGistogramPanel.TabIndex = 1;
        	// 
        	// RGistogramPanel
        	// 
        	this.RGistogramPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
        	        	        	| System.Windows.Forms.AnchorStyles.Left) 
        	        	        	| System.Windows.Forms.AnchorStyles.Right)));
        	this.RGistogramPanel.BackColor = System.Drawing.Color.Black;
        	this.RGistogramPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
        	this.RGistogramPanel.Image = null;
        	this.RGistogramPanel.Location = new System.Drawing.Point(8, 8);
        	this.RGistogramPanel.Name = "RGistogramPanel";
        	this.RGistogramPanel.Size = new System.Drawing.Size(287, 93);
        	this.RGistogramPanel.TabIndex = 0;
        	// 
        	// BGistogramPanel
        	// 
        	this.BGistogramPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
        	        	        	| System.Windows.Forms.AnchorStyles.Left) 
        	        	        	| System.Windows.Forms.AnchorStyles.Right)));
        	this.BGistogramPanel.BackColor = System.Drawing.Color.Black;
        	this.BGistogramPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
        	this.BGistogramPanel.Image = null;
        	this.BGistogramPanel.Location = new System.Drawing.Point(8, 206);
        	this.BGistogramPanel.Name = "BGistogramPanel";
        	this.BGistogramPanel.Size = new System.Drawing.Size(287, 95);
        	this.BGistogramPanel.TabIndex = 2;
        	// 
        	// ApplyButton
        	// 
        	this.ApplyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
        	this.ApplyButton.Enabled = false;
        	this.ApplyButton.Location = new System.Drawing.Point(684, 144);
        	this.ApplyButton.Name = "ApplyButton";
        	this.ApplyButton.Size = new System.Drawing.Size(102, 35);
        	this.ApplyButton.TabIndex = 24;
        	this.ApplyButton.Text = "Применить";
        	this.ApplyButton.UseVisualStyleBackColor = true;
        	this.ApplyButton.Click += new System.EventHandler(this.ApplyButton_Click);
        	// 
        	// panel4
        	// 
        	this.panel4.BackColor = System.Drawing.SystemColors.GrayText;
        	this.panel4.Controls.Add(this.panel5);
        	this.panel4.Controls.Add(this.ApplyButton);
        	this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
        	this.panel4.Location = new System.Drawing.Point(0, 333);
        	this.panel4.Name = "panel4";
        	this.panel4.Size = new System.Drawing.Size(798, 191);
        	this.panel4.TabIndex = 10;
        	// 
        	// panel5
        	// 
        	this.panel5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
        	        	        	| System.Windows.Forms.AnchorStyles.Left) 
        	        	        	| System.Windows.Forms.AnchorStyles.Right)));
        	this.panel5.AutoScroll = true;
        	this.panel5.Controls.Add(this.tableLayoutPanel1);
        	this.panel5.Location = new System.Drawing.Point(12, 0);
        	this.panel5.Name = "panel5";
        	this.panel5.Size = new System.Drawing.Size(663, 179);
        	this.panel5.TabIndex = 10;
        	// 
        	// tableLayoutPanel1
        	// 
        	this.tableLayoutPanel1.ColumnCount = 4;
        	this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 248F));
        	this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 163F));
        	this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 185F));
        	this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 246F));
        	this.tableLayoutPanel1.Controls.Add(this.panel9, 3, 0);
        	this.tableLayoutPanel1.Controls.Add(this.panel8, 2, 0);
        	this.tableLayoutPanel1.Controls.Add(this.panel7, 1, 0);
        	this.tableLayoutPanel1.Controls.Add(this.panel6, 0, 0);
        	this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Left;
        	this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
        	this.tableLayoutPanel1.Name = "tableLayoutPanel1";
        	this.tableLayoutPanel1.RowCount = 1;
        	this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
        	this.tableLayoutPanel1.Size = new System.Drawing.Size(842, 162);
        	this.tableLayoutPanel1.TabIndex = 22;
        	// 
        	// panel9
        	// 
        	this.panel9.Controls.Add(this.ParametrLabel);
        	this.panel9.Controls.Add(this.AppertureLabel);
        	this.panel9.Controls.Add(this.AppertureComboBox);
        	this.panel9.Controls.Add(this.FilterLabel);
        	this.panel9.Controls.Add(this.ParamUpDown);
        	this.panel9.Controls.Add(this.FilterComboBox);
        	this.panel9.Controls.Add(this.FilterGroupBox);
        	this.panel9.Controls.Add(this.SizeUpDown);
        	this.panel9.Controls.Add(this.SizeLabel);
        	this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
        	this.panel9.Location = new System.Drawing.Point(596, 0);
        	this.panel9.Margin = new System.Windows.Forms.Padding(0);
        	this.panel9.Name = "panel9";
        	this.panel9.Size = new System.Drawing.Size(246, 162);
        	this.panel9.TabIndex = 4;
        	// 
        	// ParametrLabel
        	// 
        	this.ParametrLabel.AutoSize = true;
        	this.ParametrLabel.Location = new System.Drawing.Point(128, 99);
        	this.ParametrLabel.Name = "ParametrLabel";
        	this.ParametrLabel.Size = new System.Drawing.Size(17, 13);
        	this.ParametrLabel.TabIndex = 20;
        	this.ParametrLabel.Text = "P:";
        	this.ParametrLabel.Visible = false;
        	// 
        	// AppertureLabel
        	// 
        	this.AppertureLabel.AutoSize = true;
        	this.AppertureLabel.Location = new System.Drawing.Point(82, 99);
        	this.AppertureLabel.Name = "AppertureLabel";
        	this.AppertureLabel.Size = new System.Drawing.Size(63, 13);
        	this.AppertureLabel.TabIndex = 3;
        	this.AppertureLabel.Text = "Аппертура:";
        	this.AppertureLabel.Visible = false;
        	// 
        	// AppertureComboBox
        	// 
        	this.AppertureComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        	this.AppertureComboBox.FormattingEnabled = true;
        	this.AppertureComboBox.Items.AddRange(new object[] {
        	        	        	"квадранная",
        	        	        	"крестообразная",
        	        	        	"круглая"});
        	this.AppertureComboBox.Location = new System.Drawing.Point(145, 95);
        	this.AppertureComboBox.Name = "AppertureComboBox";
        	this.AppertureComboBox.Size = new System.Drawing.Size(101, 21);
        	this.AppertureComboBox.TabIndex = 22;
        	this.AppertureComboBox.Visible = false;
        	// 
        	// FilterLabel
        	// 
        	this.FilterLabel.AutoSize = true;
        	this.FilterLabel.Enabled = false;
        	this.FilterLabel.Location = new System.Drawing.Point(5, 9);
        	this.FilterLabel.Name = "FilterLabel";
        	this.FilterLabel.Size = new System.Drawing.Size(50, 13);
        	this.FilterLabel.TabIndex = 11;
        	this.FilterLabel.Text = "Фильтр:";
        	// 
        	// ParamUpDown
        	// 
        	this.ParamUpDown.DecimalPlaces = 1;
        	this.ParamUpDown.Increment = new decimal(new int[] {
        	        	        	1,
        	        	        	0,
        	        	        	0,
        	        	        	65536});
        	this.ParamUpDown.Location = new System.Drawing.Point(145, 96);
        	this.ParamUpDown.Maximum = new decimal(new int[] {
        	        	        	10,
        	        	        	0,
        	        	        	0,
        	        	        	0});
        	this.ParamUpDown.Minimum = new decimal(new int[] {
        	        	        	1,
        	        	        	0,
        	        	        	0,
        	        	        	0});
        	this.ParamUpDown.Name = "ParamUpDown";
        	this.ParamUpDown.Size = new System.Drawing.Size(100, 20);
        	this.ParamUpDown.TabIndex = 21;
        	this.ParamUpDown.Value = new decimal(new int[] {
        	        	        	1,
        	        	        	0,
        	        	        	0,
        	        	        	0});
        	this.ParamUpDown.Visible = false;
        	// 
        	// FilterComboBox
        	// 
        	this.FilterComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        	this.FilterComboBox.Enabled = false;
        	this.FilterComboBox.FormattingEnabled = true;
        	this.FilterComboBox.Items.AddRange(new object[] {
        	        	        	"--нет--",
        	        	        	"Усредняющий",
        	        	        	"Треугольный",
        	        	        	"Гаусса",
        	        	        	"Превита(горизонтальный)",
        	        	        	"Превита(вертикальный)",
        	        	        	"Собеля(горизонтальный)",
        	        	        	"Собеля(вертикальный)",
        	        	        	"Высокочастотный",
        	        	        	"Поднятия высоких частот",
        	        	        	"Нерезкое маскирование",
        	        	        	"Медианный",
        	        	        	"Минимизирующий",
        	        	        	"Максимизирующий"});
        	this.FilterComboBox.Location = new System.Drawing.Point(0, 33);
        	this.FilterComboBox.Name = "FilterComboBox";
        	this.FilterComboBox.Size = new System.Drawing.Size(245, 21);
        	this.FilterComboBox.TabIndex = 17;
        	this.FilterComboBox.SelectedIndexChanged += new System.EventHandler(this.FilterComboBox_SelectedIndexChanged);
        	// 
        	// FilterGroupBox
        	// 
        	this.FilterGroupBox.Controls.Add(this.BFilterCheckBox);
        	this.FilterGroupBox.Controls.Add(this.GFilterCheckBox);
        	this.FilterGroupBox.Controls.Add(this.RFilterCheckBox);
        	this.FilterGroupBox.Location = new System.Drawing.Point(0, 61);
        	this.FilterGroupBox.Name = "FilterGroupBox";
        	this.FilterGroupBox.Size = new System.Drawing.Size(76, 90);
        	this.FilterGroupBox.TabIndex = 17;
        	this.FilterGroupBox.TabStop = false;
        	this.FilterGroupBox.Text = "Плоскость";
        	this.FilterGroupBox.Visible = false;
        	// 
        	// BFilterCheckBox
        	// 
        	this.BFilterCheckBox.AutoSize = true;
        	this.BFilterCheckBox.Checked = true;
        	this.BFilterCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
        	this.BFilterCheckBox.Location = new System.Drawing.Point(9, 62);
        	this.BFilterCheckBox.Name = "BFilterCheckBox";
        	this.BFilterCheckBox.Size = new System.Drawing.Size(33, 17);
        	this.BFilterCheckBox.TabIndex = 2;
        	this.BFilterCheckBox.Text = "B";
        	this.BFilterCheckBox.UseVisualStyleBackColor = true;
        	// 
        	// GFilterCheckBox
        	// 
        	this.GFilterCheckBox.AutoSize = true;
        	this.GFilterCheckBox.Checked = true;
        	this.GFilterCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
        	this.GFilterCheckBox.Location = new System.Drawing.Point(9, 39);
        	this.GFilterCheckBox.Name = "GFilterCheckBox";
        	this.GFilterCheckBox.Size = new System.Drawing.Size(34, 17);
        	this.GFilterCheckBox.TabIndex = 1;
        	this.GFilterCheckBox.Text = "G";
        	this.GFilterCheckBox.UseVisualStyleBackColor = true;
        	// 
        	// RFilterCheckBox
        	// 
        	this.RFilterCheckBox.AutoSize = true;
        	this.RFilterCheckBox.Checked = true;
        	this.RFilterCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
        	this.RFilterCheckBox.Location = new System.Drawing.Point(9, 19);
        	this.RFilterCheckBox.Name = "RFilterCheckBox";
        	this.RFilterCheckBox.Size = new System.Drawing.Size(34, 17);
        	this.RFilterCheckBox.TabIndex = 0;
        	this.RFilterCheckBox.Text = "R";
        	this.RFilterCheckBox.UseVisualStyleBackColor = true;
        	// 
        	// SizeUpDown
        	// 
        	this.SizeUpDown.Increment = new decimal(new int[] {
        	        	        	2,
        	        	        	0,
        	        	        	0,
        	        	        	0});
        	this.SizeUpDown.Location = new System.Drawing.Point(146, 71);
        	this.SizeUpDown.Maximum = new decimal(new int[] {
        	        	        	15,
        	        	        	0,
        	        	        	0,
        	        	        	0});
        	this.SizeUpDown.Minimum = new decimal(new int[] {
        	        	        	3,
        	        	        	0,
        	        	        	0,
        	        	        	0});
        	this.SizeUpDown.Name = "SizeUpDown";
        	this.SizeUpDown.Size = new System.Drawing.Size(99, 20);
        	this.SizeUpDown.TabIndex = 19;
        	this.SizeUpDown.Value = new decimal(new int[] {
        	        	        	3,
        	        	        	0,
        	        	        	0,
        	        	        	0});
        	this.SizeUpDown.Visible = false;
        	this.SizeUpDown.ValueChanged += new System.EventHandler(this.SizeUpDown_ValueChanged);
        	// 
        	// SizeLabel
        	// 
        	this.SizeLabel.AutoSize = true;
        	this.SizeLabel.Location = new System.Drawing.Point(96, 74);
        	this.SizeLabel.Name = "SizeLabel";
        	this.SizeLabel.Size = new System.Drawing.Size(49, 13);
        	this.SizeLabel.TabIndex = 18;
        	this.SizeLabel.Text = "Размер:";
        	this.SizeLabel.Visible = false;
        	// 
        	// panel8
        	// 
        	this.panel8.Controls.Add(this.GradTransfLabel);
        	this.panel8.Controls.Add(this.GradTransfComboBox);
        	this.panel8.Controls.Add(this.GradTransfGroupBox);
        	this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
        	this.panel8.Location = new System.Drawing.Point(411, 0);
        	this.panel8.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
        	this.panel8.Name = "panel8";
        	this.panel8.Size = new System.Drawing.Size(175, 162);
        	this.panel8.TabIndex = 3;
        	// 
        	// GradTransfLabel
        	// 
        	this.GradTransfLabel.AutoSize = true;
        	this.GradTransfLabel.Enabled = false;
        	this.GradTransfLabel.Location = new System.Drawing.Point(6, 9);
        	this.GradTransfLabel.Name = "GradTransfLabel";
        	this.GradTransfLabel.Size = new System.Drawing.Size(165, 13);
        	this.GradTransfLabel.TabIndex = 14;
        	this.GradTransfLabel.Text = "Градационые преобразования:";
        	// 
        	// GradTransfComboBox
        	// 
        	this.GradTransfComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        	this.GradTransfComboBox.Enabled = false;
        	this.GradTransfComboBox.FormattingEnabled = true;
        	this.GradTransfComboBox.Items.AddRange(new object[] {
        	        	        	"--нет--",
        	        	        	"Негатив",
        	        	        	"Логарифмическое",
        	        	        	"Соляризация",
        	        	        	"Контраст",
        	        	        	"Повысить яркость",
        	        	        	"Понизить яркость",
        	        	        	"Эквализация"});
        	this.GradTransfComboBox.Location = new System.Drawing.Point(0, 33);
        	this.GradTransfComboBox.Name = "GradTransfComboBox";
        	this.GradTransfComboBox.Size = new System.Drawing.Size(171, 21);
        	this.GradTransfComboBox.TabIndex = 15;
        	this.GradTransfComboBox.SelectedIndexChanged += new System.EventHandler(this.GradTransfComboBox_SelectedIndexChanged);
        	// 
        	// GradTransfGroupBox
        	// 
        	this.GradTransfGroupBox.Controls.Add(this.BGradCheckBox);
        	this.GradTransfGroupBox.Controls.Add(this.GGradCheckBox);
        	this.GradTransfGroupBox.Controls.Add(this.RGradCheckBox);
        	this.GradTransfGroupBox.Location = new System.Drawing.Point(48, 61);
        	this.GradTransfGroupBox.Name = "GradTransfGroupBox";
        	this.GradTransfGroupBox.Size = new System.Drawing.Size(76, 90);
        	this.GradTransfGroupBox.TabIndex = 16;
        	this.GradTransfGroupBox.TabStop = false;
        	this.GradTransfGroupBox.Text = "Плоскость";
        	this.GradTransfGroupBox.Visible = false;
        	// 
        	// BGradCheckBox
        	// 
        	this.BGradCheckBox.AutoSize = true;
        	this.BGradCheckBox.Checked = true;
        	this.BGradCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
        	this.BGradCheckBox.Location = new System.Drawing.Point(9, 62);
        	this.BGradCheckBox.Name = "BGradCheckBox";
        	this.BGradCheckBox.Size = new System.Drawing.Size(33, 17);
        	this.BGradCheckBox.TabIndex = 2;
        	this.BGradCheckBox.Text = "B";
        	this.BGradCheckBox.UseVisualStyleBackColor = true;
        	// 
        	// GGradCheckBox
        	// 
        	this.GGradCheckBox.AutoSize = true;
        	this.GGradCheckBox.Checked = true;
        	this.GGradCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
        	this.GGradCheckBox.Location = new System.Drawing.Point(9, 39);
        	this.GGradCheckBox.Name = "GGradCheckBox";
        	this.GGradCheckBox.Size = new System.Drawing.Size(34, 17);
        	this.GGradCheckBox.TabIndex = 1;
        	this.GGradCheckBox.Text = "G";
        	this.GGradCheckBox.UseVisualStyleBackColor = true;
        	// 
        	// RGradCheckBox
        	// 
        	this.RGradCheckBox.AutoSize = true;
        	this.RGradCheckBox.Checked = true;
        	this.RGradCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
        	this.RGradCheckBox.Location = new System.Drawing.Point(9, 19);
        	this.RGradCheckBox.Name = "RGradCheckBox";
        	this.RGradCheckBox.Size = new System.Drawing.Size(34, 17);
        	this.RGradCheckBox.TabIndex = 0;
        	this.RGradCheckBox.Text = "R";
        	this.RGradCheckBox.UseVisualStyleBackColor = true;
        	// 
        	// panel7
        	// 
        	this.panel7.Controls.Add(this.MultiplierUpDown);
        	this.panel7.Controls.Add(this.OperationsLabel);
        	this.panel7.Controls.Add(this.OperationsComboBox);
        	this.panel7.Controls.Add(this.FirstGroupBox);
        	this.panel7.Controls.Add(this.SecondGroupBox);
        	this.panel7.Controls.Add(this.OperationSign);
        	this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
        	this.panel7.Location = new System.Drawing.Point(248, 0);
        	this.panel7.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
        	this.panel7.Name = "panel7";
        	this.panel7.Size = new System.Drawing.Size(153, 162);
        	this.panel7.TabIndex = 2;
        	// 
        	// MultiplierUpDown
        	// 
        	this.MultiplierUpDown.DecimalPlaces = 3;
        	this.MultiplierUpDown.Increment = new decimal(new int[] {
        	        	        	1,
        	        	        	0,
        	        	        	0,
        	        	        	196608});
        	this.MultiplierUpDown.Location = new System.Drawing.Point(85, 98);
        	this.MultiplierUpDown.Maximum = new decimal(new int[] {
        	        	        	10,
        	        	        	0,
        	        	        	0,
        	        	        	0});
        	this.MultiplierUpDown.Minimum = new decimal(new int[] {
        	        	        	1,
        	        	        	0,
        	        	        	0,
        	        	        	65536});
        	this.MultiplierUpDown.Name = "MultiplierUpDown";
        	this.MultiplierUpDown.Size = new System.Drawing.Size(65, 20);
        	this.MultiplierUpDown.TabIndex = 13;
        	this.MultiplierUpDown.Value = new decimal(new int[] {
        	        	        	1,
        	        	        	0,
        	        	        	0,
        	        	        	0});
        	this.MultiplierUpDown.Visible = false;
        	// 
        	// OperationsLabel
        	// 
        	this.OperationsLabel.AutoSize = true;
        	this.OperationsLabel.Enabled = false;
        	this.OperationsLabel.Location = new System.Drawing.Point(9, 9);
        	this.OperationsLabel.Name = "OperationsLabel";
        	this.OperationsLabel.Size = new System.Drawing.Size(60, 13);
        	this.OperationsLabel.TabIndex = 10;
        	this.OperationsLabel.Text = "Операции:";
        	// 
        	// OperationsComboBox
        	// 
        	this.OperationsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        	this.OperationsComboBox.Enabled = false;
        	this.OperationsComboBox.FormattingEnabled = true;
        	this.OperationsComboBox.Items.AddRange(new object[] {
        	        	        	"--нет--",
        	        	        	"Присвоить плоскости",
        	        	        	"Сложить плоскости",
        	        	        	"Вычесть плоскости",
        	        	        	"Перемножить плоскости",
        	        	        	"Умножить на скаляр"});
        	this.OperationsComboBox.Location = new System.Drawing.Point(2, 31);
        	this.OperationsComboBox.Name = "OperationsComboBox";
        	this.OperationsComboBox.Size = new System.Drawing.Size(152, 21);
        	this.OperationsComboBox.TabIndex = 3;
        	this.OperationsComboBox.SelectedIndexChanged += new System.EventHandler(this.OperationsComboBox_SelectedIndexChanged);
        	// 
        	// FirstGroupBox
        	// 
        	this.FirstGroupBox.Controls.Add(this.FirstB_RadioBut);
        	this.FirstGroupBox.Controls.Add(this.FirstG_RadioBut);
        	this.FirstGroupBox.Controls.Add(this.FirstR_RadioBut);
        	this.FirstGroupBox.Location = new System.Drawing.Point(2, 61);
        	this.FirstGroupBox.Name = "FirstGroupBox";
        	this.FirstGroupBox.Size = new System.Drawing.Size(64, 90);
        	this.FirstGroupBox.TabIndex = 4;
        	this.FirstGroupBox.TabStop = false;
        	this.FirstGroupBox.Text = "Пл-ть";
        	this.FirstGroupBox.Visible = false;
        	// 
        	// FirstB_RadioBut
        	// 
        	this.FirstB_RadioBut.AutoSize = true;
        	this.FirstB_RadioBut.Location = new System.Drawing.Point(9, 63);
        	this.FirstB_RadioBut.Name = "FirstB_RadioBut";
        	this.FirstB_RadioBut.Size = new System.Drawing.Size(32, 17);
        	this.FirstB_RadioBut.TabIndex = 2;
        	this.FirstB_RadioBut.Text = "B";
        	this.FirstB_RadioBut.UseVisualStyleBackColor = true;
        	// 
        	// FirstG_RadioBut
        	// 
        	this.FirstG_RadioBut.AutoSize = true;
        	this.FirstG_RadioBut.Location = new System.Drawing.Point(9, 40);
        	this.FirstG_RadioBut.Name = "FirstG_RadioBut";
        	this.FirstG_RadioBut.Size = new System.Drawing.Size(33, 17);
        	this.FirstG_RadioBut.TabIndex = 1;
        	this.FirstG_RadioBut.Text = "G";
        	this.FirstG_RadioBut.UseVisualStyleBackColor = true;
        	// 
        	// FirstR_RadioBut
        	// 
        	this.FirstR_RadioBut.AutoSize = true;
        	this.FirstR_RadioBut.Checked = true;
        	this.FirstR_RadioBut.Location = new System.Drawing.Point(9, 19);
        	this.FirstR_RadioBut.Name = "FirstR_RadioBut";
        	this.FirstR_RadioBut.Size = new System.Drawing.Size(33, 17);
        	this.FirstR_RadioBut.TabIndex = 0;
        	this.FirstR_RadioBut.TabStop = true;
        	this.FirstR_RadioBut.Text = "R";
        	this.FirstR_RadioBut.UseVisualStyleBackColor = true;
        	// 
        	// SecondGroupBox
        	// 
        	this.SecondGroupBox.Controls.Add(this.SecondB_RadioBut);
        	this.SecondGroupBox.Controls.Add(this.SecondG_RadioBut);
        	this.SecondGroupBox.Controls.Add(this.SecondR_RadioBut);
        	this.SecondGroupBox.Location = new System.Drawing.Point(85, 61);
        	this.SecondGroupBox.Name = "SecondGroupBox";
        	this.SecondGroupBox.Size = new System.Drawing.Size(65, 91);
        	this.SecondGroupBox.TabIndex = 5;
        	this.SecondGroupBox.TabStop = false;
        	this.SecondGroupBox.Text = "Пл-ть";
        	this.SecondGroupBox.Visible = false;
        	// 
        	// SecondB_RadioBut
        	// 
        	this.SecondB_RadioBut.AutoSize = true;
        	this.SecondB_RadioBut.Location = new System.Drawing.Point(9, 63);
        	this.SecondB_RadioBut.Name = "SecondB_RadioBut";
        	this.SecondB_RadioBut.Size = new System.Drawing.Size(32, 17);
        	this.SecondB_RadioBut.TabIndex = 2;
        	this.SecondB_RadioBut.Text = "B";
        	this.SecondB_RadioBut.UseVisualStyleBackColor = true;
        	// 
        	// SecondG_RadioBut
        	// 
        	this.SecondG_RadioBut.AutoSize = true;
        	this.SecondG_RadioBut.Location = new System.Drawing.Point(9, 40);
        	this.SecondG_RadioBut.Name = "SecondG_RadioBut";
        	this.SecondG_RadioBut.Size = new System.Drawing.Size(33, 17);
        	this.SecondG_RadioBut.TabIndex = 1;
        	this.SecondG_RadioBut.Text = "G";
        	this.SecondG_RadioBut.UseVisualStyleBackColor = true;
        	// 
        	// SecondR_RadioBut
        	// 
        	this.SecondR_RadioBut.AutoSize = true;
        	this.SecondR_RadioBut.Checked = true;
        	this.SecondR_RadioBut.Location = new System.Drawing.Point(9, 19);
        	this.SecondR_RadioBut.Name = "SecondR_RadioBut";
        	this.SecondR_RadioBut.Size = new System.Drawing.Size(33, 17);
        	this.SecondR_RadioBut.TabIndex = 0;
        	this.SecondR_RadioBut.TabStop = true;
        	this.SecondR_RadioBut.Text = "R";
        	this.SecondR_RadioBut.UseVisualStyleBackColor = true;
        	// 
        	// OperationSign
        	// 
        	this.OperationSign.AutoSize = true;
        	this.OperationSign.Location = new System.Drawing.Point(69, 102);
        	this.OperationSign.Name = "OperationSign";
        	this.OperationSign.Size = new System.Drawing.Size(13, 13);
        	this.OperationSign.TabIndex = 0;
        	this.OperationSign.Text = "?";
        	this.OperationSign.Visible = false;
        	// 
        	// panel6
        	// 
        	this.panel6.Controls.Add(this.BackgroundsPanel);
        	this.panel6.Controls.Add(this.BackgroundsLabel);
        	this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
        	this.panel6.Location = new System.Drawing.Point(0, 0);
        	this.panel6.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
        	this.panel6.Name = "panel6";
        	this.panel6.Size = new System.Drawing.Size(238, 162);
        	this.panel6.TabIndex = 1;
        	// 
        	// BackgroundsPanel
        	// 
        	this.BackgroundsPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
        	        	        	| System.Windows.Forms.AnchorStyles.Left) 
        	        	        	| System.Windows.Forms.AnchorStyles.Right)));
        	this.BackgroundsPanel.AutoScroll = true;
        	this.BackgroundsPanel.BackColor = System.Drawing.SystemColors.ControlLightLight;
        	this.BackgroundsPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
        	this.BackgroundsPanel.Controls.Add(this.pictureLabel5);
        	this.BackgroundsPanel.Controls.Add(this.pictureLabel4);
        	this.BackgroundsPanel.Controls.Add(this.pictureLabel3);
        	this.BackgroundsPanel.Controls.Add(this.pictureLabel2);
        	this.BackgroundsPanel.Controls.Add(this.pictureLabel1);
        	this.BackgroundsPanel.Enabled = false;
        	this.BackgroundsPanel.Location = new System.Drawing.Point(0, 31);
        	this.BackgroundsPanel.Name = "BackgroundsPanel";
        	this.BackgroundsPanel.Size = new System.Drawing.Size(239, 131);
        	this.BackgroundsPanel.TabIndex = 8;
        	this.BackgroundsPanel.TabStop = true;
        	// 
        	// pictureLabel5
        	// 
        	this.pictureLabel5.Dock = System.Windows.Forms.DockStyle.Top;
        	this.pictureLabel5.LabelText = "Штукатурка";
        	this.pictureLabel5.Location = new System.Drawing.Point(0, 328);
        	this.pictureLabel5.Name = "pictureLabel5";
        	this.pictureLabel5.Picture = global::ImagesEqualizerApplication.Properties.Resources.Plaster;
        	this.pictureLabel5.Size = new System.Drawing.Size(218, 82);
        	this.pictureLabel5.TabIndex = 2;
        	this.pictureLabel5.TabStop = false;
        	this.pictureLabel5.MouseClick += new System.Windows.Forms.MouseEventHandler(this.PictureLabel_MouseClick);
        	// 
        	// pictureLabel4
        	// 
        	this.pictureLabel4.Dock = System.Windows.Forms.DockStyle.Top;
        	this.pictureLabel4.LabelText = "Ночной ковыль";
        	this.pictureLabel4.Location = new System.Drawing.Point(0, 246);
        	this.pictureLabel4.Name = "pictureLabel4";
        	this.pictureLabel4.Picture = global::ImagesEqualizerApplication.Properties.Resources.NightFeatherGrass;
        	this.pictureLabel4.Size = new System.Drawing.Size(218, 82);
        	this.pictureLabel4.TabIndex = 2;
        	this.pictureLabel4.TabStop = false;
        	this.pictureLabel4.MouseClick += new System.Windows.Forms.MouseEventHandler(this.PictureLabel_MouseClick);
        	// 
        	// pictureLabel3
        	// 
        	this.pictureLabel3.Dock = System.Windows.Forms.DockStyle.Top;
        	this.pictureLabel3.LabelText = "Лён";
        	this.pictureLabel3.Location = new System.Drawing.Point(0, 164);
        	this.pictureLabel3.Name = "pictureLabel3";
        	this.pictureLabel3.Picture = global::ImagesEqualizerApplication.Properties.Resources.Fief;
        	this.pictureLabel3.Size = new System.Drawing.Size(218, 82);
        	this.pictureLabel3.TabIndex = 2;
        	this.pictureLabel3.TabStop = false;
        	this.pictureLabel3.MouseClick += new System.Windows.Forms.MouseEventHandler(this.PictureLabel_MouseClick);
        	// 
        	// pictureLabel2
        	// 
        	this.pictureLabel2.Dock = System.Windows.Forms.DockStyle.Top;
        	this.pictureLabel2.LabelText = "Пузыри";
        	this.pictureLabel2.Location = new System.Drawing.Point(0, 82);
        	this.pictureLabel2.Name = "pictureLabel2";
        	this.pictureLabel2.Picture = global::ImagesEqualizerApplication.Properties.Resources.Bubbles;
        	this.pictureLabel2.Size = new System.Drawing.Size(218, 82);
        	this.pictureLabel2.TabIndex = 2;
        	this.pictureLabel2.TabStop = false;
        	this.pictureLabel2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.PictureLabel_MouseClick);
        	// 
        	// pictureLabel1
        	// 
        	this.pictureLabel1.Dock = System.Windows.Forms.DockStyle.Top;
        	this.pictureLabel1.LabelText = "Зелёный камень";
        	this.pictureLabel1.Location = new System.Drawing.Point(0, 0);
        	this.pictureLabel1.Name = "pictureLabel1";
        	this.pictureLabel1.Picture = global::ImagesEqualizerApplication.Properties.Resources.GreenStone;
        	this.pictureLabel1.Size = new System.Drawing.Size(218, 82);
        	this.pictureLabel1.TabIndex = 2;
        	this.pictureLabel1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.PictureLabel_MouseClick);
        	// 
        	// BackgroundsLabel
        	// 
        	this.BackgroundsLabel.AutoSize = true;
        	this.BackgroundsLabel.Enabled = false;
        	this.BackgroundsLabel.Location = new System.Drawing.Point(7, 9);
        	this.BackgroundsLabel.Name = "BackgroundsLabel";
        	this.BackgroundsLabel.Size = new System.Drawing.Size(128, 13);
        	this.BackgroundsLabel.TabIndex = 9;
        	this.BackgroundsLabel.Text = "Фоновое изображение:";
        	// 
        	// EqualizerForm
        	// 
        	this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        	this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        	this.BackColor = System.Drawing.SystemColors.GrayText;
        	this.ClientSize = new System.Drawing.Size(798, 524);
        	this.Controls.Add(this.panel2);
        	this.Controls.Add(this.panel4);
        	this.Controls.Add(this.menuStrip1);
        	this.MainMenuStrip = this.menuStrip1;
        	this.MinimumSize = new System.Drawing.Size(673, 553);
        	this.Name = "EqualizerForm";
        	this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
        	this.Text = "Эквалайзер изображений";
        	this.Load += new System.EventHandler(this.EqualizerForm_Load);
        	this.menuStrip1.ResumeLayout(false);
        	this.menuStrip1.PerformLayout();
        	this.panel1.ResumeLayout(false);
        	this.panel1.PerformLayout();
        	this.ImagePanel.ResumeLayout(false);
        	this.panel2.ResumeLayout(false);
        	this.panel3.ResumeLayout(false);
        	this.GistogramsTableLayout.ResumeLayout(false);
        	this.panel4.ResumeLayout(false);
        	this.panel5.ResumeLayout(false);
        	this.tableLayoutPanel1.ResumeLayout(false);
        	this.panel9.ResumeLayout(false);
        	this.panel9.PerformLayout();
        	((System.ComponentModel.ISupportInitialize)(this.ParamUpDown)).EndInit();
        	this.FilterGroupBox.ResumeLayout(false);
        	this.FilterGroupBox.PerformLayout();
        	((System.ComponentModel.ISupportInitialize)(this.SizeUpDown)).EndInit();
        	this.panel8.ResumeLayout(false);
        	this.panel8.PerformLayout();
        	this.GradTransfGroupBox.ResumeLayout(false);
        	this.GradTransfGroupBox.PerformLayout();
        	this.panel7.ResumeLayout(false);
        	this.panel7.PerformLayout();
        	((System.ComponentModel.ISupportInitialize)(this.MultiplierUpDown)).EndInit();
        	this.FirstGroupBox.ResumeLayout(false);
        	this.FirstGroupBox.PerformLayout();
        	this.SecondGroupBox.ResumeLayout(false);
        	this.SecondGroupBox.PerformLayout();
        	this.panel6.ResumeLayout(false);
        	this.panel6.PerformLayout();
        	this.BackgroundsPanel.ResumeLayout(false);
        	this.ResumeLayout(false);
        	this.PerformLayout();
        }
        private System.ComponentModel.BackgroundWorker LoadImageBkgWorker;

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem FileMenuItem;
        private WinControls.PanelEx ImagePanel;
        private System.Windows.Forms.ToolStripMenuItem OpenMenuItem;
        private System.Windows.Forms.ToolStripMenuItem SaveMenuItem;
        private System.Windows.Forms.ToolStripMenuItem SaveAsMenuItem;
        private System.Windows.Forms.CheckBox ScaleCheckBox;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button ApplyButton;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TableLayoutPanel GistogramsTableLayout;
        private WinControls.PanelEx RGistogramPanel;
        private WinControls.PanelEx GGistogramPanel;
        private WinControls.PanelEx BGistogramPanel;
        private System.Windows.Forms.ProgressBar WaitProgressBar;
        private System.Windows.Forms.NumericUpDown ParamUpDown;
        private System.Windows.Forms.Label ParametrLabel;
        private System.Windows.Forms.NumericUpDown SizeUpDown;
        private System.Windows.Forms.Label SizeLabel;
        private System.Windows.Forms.GroupBox FilterGroupBox;
        private System.Windows.Forms.CheckBox BFilterCheckBox;
        private System.Windows.Forms.CheckBox GFilterCheckBox;
        private System.Windows.Forms.CheckBox RFilterCheckBox;
        private System.Windows.Forms.ComboBox FilterComboBox;
        private System.Windows.Forms.Label FilterLabel;
        private System.Windows.Forms.GroupBox GradTransfGroupBox;
        private System.Windows.Forms.CheckBox BGradCheckBox;
        private System.Windows.Forms.CheckBox GGradCheckBox;
        private System.Windows.Forms.CheckBox RGradCheckBox;
        private System.Windows.Forms.ComboBox GradTransfComboBox;
        private System.Windows.Forms.Label GradTransfLabel;
        private System.Windows.Forms.Label OperationSign;
        private System.Windows.Forms.GroupBox SecondGroupBox;
        private System.Windows.Forms.NumericUpDown MultiplierUpDown;
        private System.Windows.Forms.RadioButton SecondB_RadioBut;
        private System.Windows.Forms.RadioButton SecondG_RadioBut;
        private System.Windows.Forms.RadioButton SecondR_RadioBut;
        private System.Windows.Forms.GroupBox FirstGroupBox;
        private System.Windows.Forms.RadioButton FirstB_RadioBut;
        private System.Windows.Forms.RadioButton FirstG_RadioBut;
        private System.Windows.Forms.RadioButton FirstR_RadioBut;
        private System.Windows.Forms.Label OperationsLabel;
        private System.Windows.Forms.ComboBox OperationsComboBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel BackgroundsPanel;
        private WinControls.PictureLabel pictureLabel5;
        private WinControls.PictureLabel pictureLabel4;
        private WinControls.PictureLabel pictureLabel3;
        private WinControls.PictureLabel pictureLabel2;
        private WinControls.PictureLabel pictureLabel1;
        private System.Windows.Forms.Label BackgroundsLabel;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.ToolStripMenuItem помощьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem AboutMenuItem;
        private System.Windows.Forms.ComboBox AppertureComboBox;
        private System.Windows.Forms.Label AppertureLabel;
        private System.Windows.Forms.ToolStripMenuItem imageMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ColorSchemasMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ChangeSizeMenuItem;
    }
}

